<!DOCTYPE html>
<html>

<head>

<?php include "template/head.php"; ?>

</head>

<body>
    <?php include "template/topbar.php"; ?>

    <div class="parent-wrapper toggled" id="outer-wrapper">

        <?php include "template/sidebar.php"; ?>

        <!-- MAIN CONTENT -->
        <div class="main-content" id="content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <h5 class="page-title"><i class="fa fa-users"></i><?php echo $title ?></h5>
                        <div class="section-divider"></div>              
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <div class="col-lg-12">
                            <div class="dash-item first-dash-item">
                                <div class="inner-item" style="height:600px">

                                        <form action="Rapot/download" method="post">
                                            <div class="modal-body">
                                                <div class="col-sm-12">
                                                    <label class="col-form-label col-2">Kelas</label>
                                                    <select class="form-control col-9" name="kelas_map_id" required="">
                                                    <?php foreach ($global['kelas'] as $value): ?>
                                                        <option value="<?php echo $value['kelas_map_id'] ?>"><?php echo $value['kelas'] ?></option>
                                                    <?php endforeach ?>
                                                    </select>                    
                                                </div>
                                                <div class="col-sm-12">
                                                    <label class="col-form-label col-2">Semester</label>
                                                    <select class="form-control col-9" name="semester_id" required="">
                                                    <?php foreach ($semester['semester'] as $value): ?>
                                                        <option value="<?php echo $value['data_combo_id'] ?>"><?php echo $value['nama'] ?></option>
                                                    <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="modal-footer ">
                                                <div class="table-action-box text-center">
                                                    <button type="submit" class="btn btn-primary">Download</button>
                                                </div>
                                            </div>
                                        </form>
                                                                
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-togggle-btn">
                <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
            </div>
            <div class="dash-footer col-lg-12">
                <p>SD Kristen Sokaraja</p>
            </div>
        </div>
    </div>
    <?php include "template/scripts.php"; ?>
</body>
</html>