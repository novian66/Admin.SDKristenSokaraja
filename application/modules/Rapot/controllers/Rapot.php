<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rapot extends MX_Controller {
	public function __construct()
  	{
    	parent::__construct();
    	$this->c_auth->checkLogin();
		$this->admin = $this->c_auth->getAdmin();
		$this->global = $this->c_global->data_list();
		$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
		$this->global_semester = $this->c_global->list_semester();
    $this->load->model('m_tahun_ajar');
    $this->load->model('m_siswa_kelas');
    $this->load->model('m_nilai');
  	}

  	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;
		$data['title'] = 'Download Rapot';
	  $data['list_is_active'] = $this->m_tahun_ajar->get_list_is_active();
		$this->load->view('rapot',$data);
	}

	public function download(){
		$filter = $_POST;
		$ta_aktif = $this->db->get_where('tahun_ajar', array('status_id' => '13'))->result_array();
		$semester = $this->db->get_where('data_combo', array('data_combo_id' => $_POST['semester_id']))->result_array();
		$tahun_ajar_id = $this->session->userdata('tahun_ajar_id');
		// print_r($tahun_ajar_id);die();
		$siswa = $this->m_siswa_kelas->get_list_siswa($tahun_ajar_id,$_POST['kelas_map_id']);

		

	 error_reporting(E_ALL);
	 ini_set('display_errors', TRUE);
	 ini_set('display_startup_errors', TRUE);
	 date_default_timezone_set('Europe/London');

	 define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

	 /** Include PHPExcel */
	 require_once('assets/plugins/PHPExcel/Classes/PHPExcel.php');


	 // Create new PHPExcel object
	 $objPHPExcel = new PHPExcel();

	 // Set document properties
	 $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
								 ->setLastModifiedBy("Maarten Balliauw")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("Test result file");
		$indx = 0;
		foreach($siswa as $key=>$val){
			$objWorkSheet = $objPHPExcel->createSheet($indx); //Setting index when creating

			//Write cells
			$baris=1;
			$objWorkSheet->mergeCells("A$baris:F$baris");
			$objWorkSheet->setCellValue("A$baris", 'LAPORAN HASIL BELAJAR SISWA');
			$baris++;
			$objWorkSheet->mergeCells("A$baris:F$baris");
			$objWorkSheet->setCellValue("A$baris", 'ULANGAN TENGAH SEMESTER');
			$baris++;
			$baris++;
			$objWorkSheet->mergeCells("A$baris:B$baris");
			$objWorkSheet->setCellValue("A$baris", 'NAMA SISWA');
			$objWorkSheet->mergeCells("C$baris:D$baris");
			$objWorkSheet->setCellValue("C$baris", ': '.$val['nama']);
			$objWorkSheet->setCellValue("E$baris", 'KELAS');
			$objWorkSheet->setCellValue("F$baris", ': '.$val['kelas']);
			$baris++;
			$objWorkSheet->mergeCells("A$baris:B$baris");
			$objWorkSheet->setCellValue("A$baris", 'NIS');
			$objWorkSheet->mergeCells("C$baris:D$baris");
			$objWorkSheet->setCellValue("C$baris", ': '.$val['nis']);
			$objWorkSheet->setCellValue("E$baris", 'SEMESTER');
			$objWorkSheet->setCellValue("F$baris", ': '.$semester[0]['nama']);
			$baris++;
			$objWorkSheet->mergeCells("A$baris:B$baris");
			$objWorkSheet->setCellValue("A$baris", 'NAMA SEKOLAH');
			$objWorkSheet->mergeCells("C$baris:D$baris");
			$objWorkSheet->setCellValue("C$baris", ': '.'SD KRISTEN SOKARAJA');
			$objWorkSheet->setCellValue("E$baris", 'TAHUN');
			$objWorkSheet->setCellValue("F$baris", ': '.$ta_aktif[0]['nama']);
			$baris++;
			$objWorkSheet->mergeCells("A$baris:B$baris");
			$objWorkSheet->setCellValue("A$baris", 'ALAMAT SEKOLAH');
			$objWorkSheet->mergeCells("C$baris:D$baris");
			$objWorkSheet->setCellValue("C$baris", ': '.'SOKARAJA');
			$baris++;
			$baris++;
			$barisTemp = $baris;
			$cellAtas = $baris;
			$objWorkSheet->mergeCells("D$baris:E$baris");
			$objWorkSheet->setCellValue("D$baris", 'NILAI PRESTASI');
			$baris++;
			$objWorkSheet->mergeCells("A$barisTemp:A$baris");
			$objWorkSheet->setCellValue("A$barisTemp", 'No');
			$objWorkSheet->mergeCells("B$barisTemp:B$baris");
			$objWorkSheet->setCellValue("B$barisTemp", 'Mata Pelajaran');
			$objWorkSheet->mergeCells("C$barisTemp:C$baris");
			$objWorkSheet->setCellValue("C$barisTemp", 'KKM');
			$objWorkSheet->setCellValue("D$baris", 'Nilai');
			$objWorkSheet->setCellValue("E$baris", 'Nilai Huruf');
			$objWorkSheet->mergeCells("F$barisTemp:F$baris");
			$objWorkSheet->setCellValue("F$barisTemp", 'Keterangan');						
			$baris++;	
			$pelajaran = $this->m_nilai->raport($val['siswa_id'],$_POST['semester_id'],$tahun_ajar_id);
			$no =1 ;
			$cellBawah = $baris;
			foreach($pelajaran as $val2){
				$objWorkSheet->setCellValue("A$baris", $no);
				$objWorkSheet->setCellValue("B$baris", $val2['pelajaran']);
				$objWorkSheet->setCellValue("C$baris", $val2['kkm']);
				$objWorkSheet->setCellValue("D$baris", $val2['nilai']);
				$objWorkSheet->setCellValue("E$baris", $val2['nilai']);
				$objWorkSheet->setCellValue("F$baris", $val2['keterangan']);
				$cellBawah = $baris;
				$baris++;
				$no++;
			}


			$horizontalCenter = array(
				'alignment' => array(
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
			);
	 
			$centerAllBold = array(
				'alignment' => array(
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				),
				'font' => array(
					'bold' => true,
				)
			);	
			
			$borderAll = array(
					'borders' => array(
							'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN
							)
					)
			);		

			$objWorkSheet->getStyle("A1:F2")->applyFromArray($centerAllBold);
			$objWorkSheet->getStyle("A9:F10")->applyFromArray($centerAllBold);
			$objPHPExcel->getActiveSheet()->getStyle("A$cellAtas:F$cellBawah")->applyFromArray($borderAll);

			$objWorkSheet->getColumnDimension('A')->setWidth(2.86+0.71);
			$objWorkSheet->getColumnDimension('B')->setWidth(24.57+0.71);
			$objWorkSheet->getColumnDimension('C')->setWidth(14.71+0.71);
			$objWorkSheet->getColumnDimension('D')->setWidth(15.43+0.71);
			$objWorkSheet->getColumnDimension('E')->setWidth(19.14+0.71);
			$objWorkSheet->getColumnDimension('F')->setWidth(29.43+0.71);
			// Rename sheet
			$objWorkSheet->setTitle($val['nama']);
			$indx++;
		}		 

		$objPHPExcel->setActiveSheetIndex(0);


	 // Redirect output to a client’s web browser (Excel2007)
	 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	 header('Content-Disposition: attachment;filename="Raport.xlsx"');
	 header('Cache-Control: max-age=0');
	 // If you're serving to IE 9, then the following may be needed
	 header('Cache-Control: max-age=1');

	 // If you're serving to IE over SSL, then the following may be needed
	 header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	 header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	 header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	 header ('Pragma: public'); // HTTP/1.0

	 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	 $objWriter->save('php://output');
	 exit;

	}
}
