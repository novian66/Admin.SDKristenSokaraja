<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun_ajar extends MX_Controller {
	public function __construct()
  	{
    	parent::__construct();
    	$this->c_auth->checkLogin();
		$this->admin = $this->c_auth->getAdmin();
		$this->global = $this->c_global->data_list();
		$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
		$this->global_semester = $this->c_global->list_semester();
    $this->load->model('m_tahun_ajar');
  	}

  	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;
		$data['title'] = 'Tahun Ajar';
	    $data['tahun_ajarr'] = $this->m_tahun_ajar->data_tahun_ajar();
	    foreach ($data['tahun_ajarr'] as $key => $val) {
	      	$data['tahun_ajar'][$val['tahun_ajar_id']] = $val;
      		unset($val['tahun_ajar'][$key]);	
	    }
	    $data['list_is_active'] = $this->m_tahun_ajar->get_list_is_active();
		$this->load->view('tahun_ajar',$data);
	}

	public function do_input(){
			$post = $this->input->post();
			if($post['status_id']==13){
				$this->db->update('tahun_ajar', array('status_id'=> 12));
			}			
	    $insert = array(
			'nama'         	=> $post['nama'],
			'status_id' 	=> $post['status_id']
	    );
	    $result_insert = $this->m_tahun_ajar->insert($insert);
	    if ($result_insert) {
	      	$this->session->set_flashdata('success', 'Data success insert!');
	    } else {
	        $this->session->set_flashdata('error', 'Data cannot insert!');
	    }
	    redirect(base_url().'tahun_ajar');
  	}

  	public function do_update() {
			$post = $this->input->post();
			if($post['status_id']==13){
				$this->db->update('tahun_ajar', array('status_id'=> 12));
			}
	    $id = $post['tahun_ajar_id'];
	    $data_update = array(
			'nama'         	=> $post['nama'],
			'status_id' 	=> $post['status_id']
	    );
	    $result_update = $this->m_tahun_ajar->update($id, $data_update);
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Data success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Data cannot updated!');
	  	}
	    redirect(base_url().'tahun_ajar');
  	}

  	public function do_delete($id) {
	    $result_delete = $this->m_tahun_ajar->delete($id);
	    if ($result_delete) {
	      	$this->session->set_flashdata('success', 'Data success deleted!');
	  	} else {
		    $this->session->set_flashdata('error', 'Data cannot deleted!');
	  	}
	    redirect(base_url().'tahun_ajar');
  	}
}
