<!DOCTYPE html>
<html>

<head>

<?php include "template/head.php"; ?>

</head>

<body>
    <?php include "template/topbar.php"; ?>

    <div class="parent-wrapper toggled" id="outer-wrapper">

        <?php include "template/sidebar.php"; ?>

        <!-- MAIN CONTENT -->
        <div class="main-content" id="content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <h5 class="page-title"><i class="fa fa-users"></i>Wali Kelas <?php echo $admin['tahun_ajar'] ?></h5>
                        <div class="section-divider"></div>
                        <?php
                        $message = $this->session->flashdata('message');
                        if ($message) {
                            ?>
                            <div class="alert alert-<?php echo $message['condition'] ?>"><i class="icon icon_error-circle_alt"></i><?php echo $message['text'] ?></div>
                        <?php } ?>                         
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <div class="col-lg-12">
                            <div class="dash-item first-dash-item">
                                <div class="inner-item">
                                    <form action="wali_kelas/simpan" method="post">
                                    <table class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Kelas</th>
                                                <th>Wali</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $no = 0;
                                                foreach($kelas as $val){
                                            ?>
                                            <tr>
                                                <td>
                                                <input type="hidden" name="kelas[<?php echo $no ?>][kelas_map_id]" value="<?php echo $val['kelas_map_id'] ?>"/>
                                                <input type="hidden" name="kelas[<?php echo $no ?>][kelas_id]" value="<?php echo $val['kelas_id'] ?>"/>
                                                <input type="hidden" name="kelas[<?php echo $no ?>][tahun_ajar_id]" value="<?php echo $val['tahun_ajar_id'] ?>"/>
                                                <?php echo $val['kelas'] ?></td>
                                                <td>
                                                    <select name="kelas[<?php echo $no ?>][karyawan_id]" class="form-control">
                                                        <option value="">-- Pilih Wali --</option>
                                                        <?php foreach($wali as $val2){
                                                            $selected="";
                                                            if($val2['karyawan_id']==$val['karyawan_id'])
                                                            $selected="selected";

                                                            echo "<option $selected value='".$val2['karyawan_id']."'>".$val2['nama']."</option>";
                                                        }?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <?php
                                            $no++;
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                    <br>
                                    <div class="text-center">
                                    <input class="btn btn-primary" type="submit" value="Simpan" />
                                            </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-togggle-btn">
                <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
            </div>
            <div class="dash-footer col-lg-12">
                <p>SD Kristen Sokaraja</p>
            </div>
        </div>
    </div>
    <?php include "template/scripts.php"; ?>

</body>

</html>