<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wali_kelas extends MX_Controller {
	public function __construct()
  {
	    parent::__construct();
	    $this->c_auth->checkLogin();
		$this->admin = $this->c_auth->getAdmin();
		$this->global = $this->c_global->data_list();
		$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
		$this->global_semester = $this->c_global->list_semester();
		$this->tahun_ajar_id = $this->session->userdata('tahun_ajar_id');
    $this->load->model('m_kelas');
  }  
	public function index()
	{
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;
		$data['kelas'] = $this->m_kelas->data_wali_kelas($this->tahun_ajar_id);
		$data['wali'] = $this->db->get_where('karyawan', array('jabatan_id' => 6))->result_array();
		$this->load->view('wali_kelas',$data);
	}

	public function simpan(){
		foreach($_POST['kelas'] as $val){
			if($val['kelas_map_id']==''){
				if(!empty($val['karyawan_id'])){
					$params = array(
						'kelas_id' => $val['kelas_id'],
						'tahun_ajar_id' => $val['tahun_ajar_id'],
						'karyawan_id' => $val['karyawan_id']
					);
					$this->db->insert('kelas_map', $params); 
				}

			}
			else{
				if(!empty($val['karyawan_id'])){
					$params = array(
						'karyawan_id' => $val['karyawan_id']
					);
					$this->db->where('kelas_map_id', $val['kelas_map_id']);
					$this->db->update('kelas_map', $params); 
				}else{
					$this->db->delete('kelas_map', array('kelas_map_id'=>$val['kelas_map_id'])); 
				}
			}
		}

		$this->session->set_flashdata('message', array('condition'=>'success','text'=>'Ubah data berhasil.'));
		$this->session->set_flashdata('content', $_POST);
		header("location: ".base_url()."wali_kelas");
	}
}
