<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi extends MX_Controller {
	public function __construct()
  	{
    	parent::__construct();
    	$this->c_auth->checkLogin();
			$this->admin = $this->c_auth->getAdmin();
			$this->global = $this->c_global->data_list();
			$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
			$this->global_semester = $this->c_global->list_semester();
    	$this->load->model('m_informasi');
  	}

  	public function index() {
			$data['admin'] = $this->admin;
			$data['global'] = $this->global;
			$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
			$data['semester'] = $this->global_semester;		
			$data['title'] = 'Informasi';
				$data['informasis'] = $this->m_informasi->data_informasi();
				$data['informasi'] = array();
				foreach ($data['informasis'] as $key => $val) {
						$data['informasi'][$val['informasi_id']] = $val;
						unset($val['informasi'][$key]);	
				}
				$data['list_is_active'] = $this->m_informasi->get_list_is_active();
				$data['list_kelas_map'] = $this->m_informasi->get_list_kelas_map();
				$data['list_kelas'] = $this->m_informasi->get_list_kelas();
				$this->load->view('informasi',$data);
		}

	public function do_input(){
	    $post = $this->input->post();
	    if ($post['kelas_map_id']=='') {
	    	$post['kelas_map_id'] = null;
	    }
	    $insert = array(
			'tanggal'       => $post['tanggal'],
			'kelas_map_id'  => $post['kelas_map_id'],
			'judul'         => $post['judul'],
			'isi'         	=> $post['isi'],
			'status_id' 		=> $post['status_id']
	    );
	    $result_insert = $this->m_informasi->insert($insert);
	    if ($result_insert) {
					$this->session->set_flashdata('success', 'Data success insert!');				
	    } else {
	        $this->session->set_flashdata('error', 'Data cannot insert!');
	    }
	    redirect(base_url().'informasi');
  }

	public function do_update() {
		$post = $this->input->post();
		$id = $post['informasi_id'];
		if ($post['kelas_map_id']=='') {
			$post['kelas_map_id'] = null;
		}
		$data_update = array(
		'tanggal'       => $post['tanggal'],
		'kelas_map_id'  => $post['kelas_map_id'],
		'judul'         => $post['judul'],
		'isi'         	=> $post['isi'],
		'status_id' 	=> $post['status_id']
		);
		$result_update = $this->m_informasi->update($id, $data_update);
		if ($result_update) {
				$this->session->set_flashdata('success', 'Data success updated!');
		} else {
				$this->session->set_flashdata('error', 'Data cannot updated!');
		}
		redirect(base_url().'informasi');
	}

	public function do_delete($id) {
		$result_delete = $this->m_informasi->delete($id);
		if ($result_delete) {
				$this->session->set_flashdata('success', 'Data success deleted!');
		} else {
			$this->session->set_flashdata('error', 'Data cannot deleted!');
		}
		redirect(base_url().'informasi');
	}

	public function do_send($id){
		require_once("assets/plugins/Html2Text.php");
		require_once("assets/plugins/Html2TextException.php");
		$sukses = '';
		$gagal = '';
		$informasi = $this->db->get_where('informasi', array('informasi_id' => $id))->result_array();
		$text = Html2Text\Html2Text::convert($informasi[0]['isi']);
		$judul = $informasi[0]['judul']; 
		$isi = $text;
		$tanggal = date('d-M-Y',strtotime($informasi[0]['tanggal'])); 
		$TOKEN  = "491911261:AAHUgmLeB3vEEh77L6ehKiN3KLssaWyk9V0";  // ganti token ini dengan token bot mu

		$target = $this->m_informasi->get_target($informasi[0]['kelas_map_id']);
		// echo "<pre>";
		// print_r($target);
		// die();

		foreach($target as $val){
			$chatid = $val['telegram']; // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
			$pesan 	= 'Hai... '.$val['nama'].' (Kelas '.$val['kelas'].')

			
			Diterbitkan informasi baru pada tanggal '.$tanggal.'


			*'.$judul.'*
			'.$isi;
			// ----------- code -------------
			$method	= "sendMessage";
			$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
			$post = [
			 'chat_id' => $chatid,
			 'parse_mode' => 'markdown', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
			 'text' => $pesan
			];
			$header = [
			 "X-Requested-With: XMLHttpRequest",
			 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
			];
			// hapus 1 baris ini:
			// die('Hapus baris ini sebelum bisa berjalan, terimakasih.');
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $url);
			//curl_setopt($ch, CURLOPT_REFERER, $refer);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$datas = curl_exec($ch);
			$error = curl_error($ch);
			$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			$debug['text'] = $pesan;
			$debug['code'] = $status;
			$debug['status'] = $error;
			$debug['respon'] = json_decode($datas, true);
			// print_r($status);
			// die();
			if($status==200){
				$sukses = $sukses.$val['nama'].', ';
			}
			else{
				$gagal =$gagal.$val['nama'].', ';
			}
		}

			$this->session->set_flashdata('success', 'Berhasil dikirim kepada '.$sukses.'<br>Gagal dikirim kepada '.$gagal);
			redirect(base_url().'informasi');
		
		// print_r($debug);		
	}

}
