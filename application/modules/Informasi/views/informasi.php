<!DOCTYPE html>
<html>

<head>

<?php include "template/head.php"; ?>

</head>

<body>
    <?php include "template/topbar.php"; ?>

    <div class="parent-wrapper toggled" id="outer-wrapper">

        <?php include "template/sidebar.php"; ?>

        <!-- MAIN CONTENT -->
        <div class="main-content" id="content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <h5 class="page-title"><i class="fa fa-users"></i><?php echo $title ?></h5>
                        <div class="section-divider"></div>                   
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <div class="col-lg-12">
                            <div class="dash-item first-dash-item">
                                <div class="inner-item">
                                    <?php
                                    $error = $this->session->flashdata('error');
                                    if ($error) {
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <?php echo $error; ?> <a href="#" class="alert-link">Error!</a>.
                                        </div> 
                                    <?php } ?>
                                    <?php
                                    $success = $this->session->flashdata('success');
                                    if ($success) {
                                        ?>
                                        <div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <?php echo $success; ?> <a href="#" class="alert-link"></a>.
                                        </div> 
                                    <?php } ?> 
                                    <a class="btn btn-primary pull-right" onclick="tambahModal()">Tambah</a><br><br>
                                    <table id="attendenceDetailedTable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Tahun Ajar</th>
                                                <th>Kelas</th>
                                                <th>Tanggal</th>
                                                <th>Judul</th>
                                                <th>Status Aktif</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach($informasi as $data) {
                                        echo "<tr>
                                                <td>".$data['tahun_ajar_name']."</td>
                                                <td>".$data['kelas_name']."</td>
                                                <td>".$data['tanggal']."</td>
                                                <td>".$data['judul']."</td>
                                                <td>".$data['name_is_active']."</td>
                                                <td class='action-link'>
                                                    <a class='edit' href='informasi/do_send/".$data['informasi_id']."'><i class='fa fa-send'></i></a>
                                                    <a class='edit' href='#' title='Edit' onclick='updateModal(".$data['informasi_id'].")'><i class='fa fa-edit'></i></a>
                                                    <a class='delete' href='informasi/do_delete/".$data['informasi_id']."' title='Delete' onclick='return confirm()'><i class='fa fa-remove'></i></a>
                                                </td>
                                            </tr>";
                                        }; ?>
                                        </tbody>
                                        </table>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-togggle-btn">
                <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
            </div>
            <div class="dash-footer col-lg-12">
                <p>SD Kristen Sokaraja</p>
            </div>
        </div>
    </div>
    <?php include "template/scripts.php"; ?>
</body>
</html>
<!--Kelas informasi-->
<div id="modalImage" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="informasi_kelas" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></i></h4>
                </div>
                    <input type="hidden" class="form-control" name="informasi_id" required="">
                <div class="modal-body">
                    <div class="col-sm-12">
                        <label class="col-form-label col-2">Tanggal</label>
                        <input type="date" class="form-control col-9" name="tanggal" required="">
                    </div>
                    <div class="col-sm-12">
                        <label class="col-form-label col-2">Kelas / Penerima</label>
                        <select class="form-control col-9" name="kelas_map_id">
                            <option value="">Semua</option>
                        <?php foreach ($list_kelas_map as $value): ?>
                            <option value="<?php echo $value['kelas_map_id'] ?>"><?php echo $value['tahun_ajar_name'] ?> - <?php echo $value['kelas_name'] ?></option>
                        <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-form-label col-2">Judul</label>
                        <input type="text" class="form-control col-9" name="judul" required="">
                    </div>
                    <div class="col-sm-12">
                        <label class="col-form-label col-2">Isi</label>
                        <textarea class="form-control col-9 content" name="isi"></textarea>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-form-label col-2">Status Aktif</label>
                        <select class="form-control col-9" name="status_id" required="">
                          <?php foreach ($list_is_active as $value): ?>
                            <option value="<?php echo $value['data_combo_id'] ?>"><?php echo $value['nama'] ?></option>
                          <?php endforeach ?>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <div class="table-action-box">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#mytable").DataTable();
        $('.content').summernote();
    });

    function tambahModal() {
        $("#modalImage").modal('show');
        $("#modalImage").find('.modal-title').text('Tambah Data');
        $("#modalImage").find('form').find('input').val('');
        $("#modalImage").find('form').find('textarea').html('');
        $("#modalImage").find('form').attr('action','informasi/do_input');
    }

    function updateModal(informasi_id) {
        $("#modalImage").modal('show');
        $("#modalImage").find('.modal-title').text('Edit Data');
        $("#modalImage").find('form').find('input').val('');
        $("#modalImage").find('form').attr('action','informasi/do_update');
        var all = JSON.stringify(<?php echo json_encode($informasi) ?>);
        var obj = jQuery.parseJSON(all);
        $("input[name='informasi_id']").val(obj[informasi_id].informasi_id);
        $("input[name='tanggal']").val(obj[informasi_id].tanggal);
        $("input[name='judul']").val(obj[informasi_id].judul);
        $("textarea[name='isi']").html(obj[informasi_id].isi);
        // initialize summernote
        $('.content').summernote();
        // and set code
        $('.content').summernote('code', obj[informasi_id].isi);
        $("select[name='kelas_map_id']").val(obj[informasi_id].kelas_map_id).attr('selected', 'selected');
        $("select[name='status_id']").val(obj[informasi_id].status_id).attr('selected', 'selected');
    }
</script>