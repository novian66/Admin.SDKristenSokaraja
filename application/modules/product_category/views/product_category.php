<!DOCTYPE html>
<html lang="en-US">
   <head>
		 <?php include('template/head.php'); ?>
   </head>
   <body>
      <div id="page-container">
				<?php include('template/sidebar.php'); ?>
				<?php include('template/header.php'); ?>
         <main id="main-container">
            <div class="content">
               <h2 class="content-heading">Product Category Referensi</h2>

                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <?php
                  $error = $this->session->flashdata('error');
                  if ($error) {
                      ?>
                      <div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                          <?php echo $error; ?> <a href="#" class="alert-link">Error!</a>.
                      </div> 
                  <?php } ?>
                  <?php
                  $success = $this->session->flashdata('success');
                  if ($success) {
                      ?>
                      <div class="alert alert-success alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                          <?php echo $success; ?> <a href="#" class="alert-link">Success!</a>.
                      </div> 
                  <?php } ?>

                    <div class="card">
                      <div class="card-header">
                        <button type="button" class="pull-right btn btn-primary mb-2 mr-1" onclick="tambahModal()">Tambah</button>
                      </div>
                      <div class="card-body">
                        <table id="mytable" class="table table-striped">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Kode</th>
                              <th>Nama</th>
                              <th width="30%">Deskripsi</th>
                              <th>Is Active</th>
                              <th>Is Show</th>
                              <th>Aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $no = 1;
                            foreach($product_category as $data)
                            {
                            echo "<tr>
                              <td align='center'>".$no."</td>
                              <td>".$data['value']."</td>
                              <td>".$data['name']."</td>
                              <td>".$data['description']."</td>
                              <td align='center'>".$data['name_is_active']."</td>
                              <td align='center'>".$data['name_is_show']."</td>
                              <td>
                                <button type='button' class='btn btn-primary' onclick='updateModal(".$data['category_id'].")'><i class='icon ti-pencil'></i></button>&nbsp
                                <a href='product_category/do_delete/".$data['category_id']."' class='btn btn-danger' onclick='return confirm()'><i class='icon ti-trash'></i></a>
                              </td>
                            </tr>";
                            $no++;
                            };
                            ?>
                          </tbody>
                        </table>
                      </div>
                    <!-- /End Hover table -->                            
                  </div>
            <!-- /End Tabs on card -->
                </div>

          </div><!-- .col -->
        </div><!-- .row -->               
    </div>

            <!-- Modal -->
            <div class="modal fade" id="modalImage" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <form method="post">
                    <div class="modal-header">
                      <h5 class="modal-title">Tambah Data</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                        <input type="hidden" class="form-control" name="category_id" required="">
                    <div class="modal-body">
                      <div class="form-group row">
                        <label class="col-form-label col-2">Kode</label>
                        <input type="text" class="form-control col-9" name="value" required="">
                      </div>             
                      <div class="form-group row">
                        <label class="col-form-label col-2">Nama</label>
                        <input type="text" class="form-control col-9" name="name" required="">
                      </div>              
                      <div class="form-group row">
                        <label class="col-form-label col-2">Deskripsi</label>
                        <textarea class="form-control col-9" name="description"></textarea>
                      </div>              
                      <div class="form-group row">
                        <label class="col-form-label col-2">Is Active</label>
                        <select class="form-control col-9" name="is_active" required="">
                          <?php foreach ($list_is_active as $value): ?>
                            <option value="<?php echo $value['data_list_id'] ?>"><?php echo $value['name'] ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>                 
                      <div class="form-group row">
                        <label class="col-form-label col-2">Is Show</label>
                        <select class="form-control col-9" name="is_show" required="">
                          <?php foreach ($list_is_active as $value): ?>
                            <option value="<?php echo $value['data_list_id'] ?>"><?php echo $value['name'] ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>        
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div><!-- .modal-footer -->
                  </form>
                </div><!-- .modal-content -->
              </div><!-- .modal-dialog .modal-lg -->
            </div><!-- .modal -->            

				 </main>
				 <?php include('template/footer.php'); ?>
      </div>
      <?php include('template/footerJs.php'); ?>
      <script>
        $(document).ready(function() {
          $("#mytable").DataTable();
        });

        function tambahModal() {
          $("#modalImage").modal('show');
          $("#modalImage").find('form').find('input').val('');
          $("#modalImage").find('form').find('textarea').val('');
          $("#modalImage").find('form').find('.modal-title').text('Tambah Data');
          $("#modalImage").find('form').attr('action','product_category/do_input');
        }

        function updateModal(category_id) {
          $("#modalImage").modal('show');
          $("#modalImage").find('form').find('input').val('');
          $("#modalImage").find('form').find('.modal-title').text('Edit Data');
          $("#modalImage").find('form').attr('action','product_category/do_update');
          var all = JSON.stringify(<?php echo json_encode($product_category) ?>);
          var obj = jQuery.parseJSON(all);
          $("input[name='category_id']").val(obj[category_id].category_id);
          $("input[name='value']").val(obj[category_id].value);
          $("input[name='name']").val(obj[category_id].name);
          $("textarea[name='description']").text(obj[category_id].description);
          $("select[name='is_active']").val(obj[category_id].is_active).attr('selected', 'selected');
          $("select[name='is_show']").val(obj[category_id].is_show).attr('selected', 'selected');
        }
      </script>
   </body>
</html>