<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_category extends MX_Controller {
	public function __construct()
  {
    parent::__construct();
    $this->c_auth->checkLogin();
    $nameClass = get_class($this);
    $this->c_auth->checkRole($nameClass);			
    $this->load->model('m_product_category');
  }  	
	public function index()
	{
    $data['product_category'] = $this->m_product_category->data_product_category();
    foreach ($data['product_category'] as $key => $val) {
      $data['product_category'][$val['category_id']] = $val;
      unset($data['product_category'][$key]);
    }
    $data['list_is_active'] = $this->m_product_category->get_list_is_active();
		$this->load->view('product_category',$data);
  }
  
  public function do_input(){
    $post = $this->input->post();
    $insert = array(
      'value'         => $post['value'],
      'name'          => $post['name'],
      'description'   => $post['description'],
      'is_active'     => $post['is_active'],
      'is_show'       => $post['is_show'],
      'created'       => date('Y-m-d H:i:s'),
      'createdby'     => $this->session->userdata('user_id')
    );
    $result_insert = $this->m_product_category->insert($insert);
    if ($result_insert) {
      $this->session->set_flashdata('success', 'Data success insert!');
      } else {
        $this->session->set_flashdata('error', 'Data cannot insert!');
      }
    redirect(base_url().'product_category');
  }

  public function do_update(){
    $post = $this->input->post();
    $id = $post['category_id'];
    $data_update = array(
      'value'       => $post['value'],
      'name'        => $post['name'],
      'description' => $post['description'],
      'is_active'   => $post['is_active'],
      'is_show'     => $post['is_show'],
      'updated'     => date('Y-m-d H:i:s'),
      'updatedby'   => $this->session->userdata('user_id')
    );

    $result_update = $this->m_product_category->update($id, $data_update);
    if ($result_update) {
      $this->session->set_flashdata('success', 'Data success updated!');
      } else {
        $this->session->set_flashdata('error', 'Data cannot updated!');
      }
    redirect(base_url().'product_category');
  }

  public function do_delete($id){
    $result_delete = $this->m_product_category->delete($id);
    if ($result_delete) {
      $this->session->set_flashdata('success', 'Data success deleted!');
      } else {
        $this->session->set_flashdata('error', 'Data cannot deleted!');
      }
    redirect(base_url().'product_category');
  }
}