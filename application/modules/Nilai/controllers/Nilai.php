<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends MX_Controller {
	public function __construct()
  	{
	    parent::__construct();
	    $this->c_auth->checkLogin();
		$this->admin = $this->c_auth->getAdmin();
		$this->global = $this->c_global->data_list();
		$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
		$this->global_semester = $this->c_global->list_semester();
		$this->load->model('m_kelas_map_pelajaran');
		$this->load->model('m_nilai');
	}  
	
	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;
		$data['kelas_map_pelajaran_id'] = $_GET['kelas_map_pelajaran_id'];
		$data['semester_id'] = $_GET['semester_id'];
		$data['kelas_map_pelajaran_check'] = $this->m_nilai->get_kelas_pelajaran($_GET['kelas_map_pelajaran_id']);
		$data['semester_check'] = $this->m_nilai->get_semester($_GET['semester_id']);
		$data['nilai'] = $this->m_nilai->get_nilai($_GET['kelas_map_pelajaran_id'], $_GET['semester_id']);
		$this->load->view('nilai', $data);
	}

  	public function do_update() {
	    $post = $this->input->post();
	    foreach ($post['siswa_id'] as $key => $value) {
	    	$data_update = array(
	    		'kelas_map_pelajaran_id'=> $post['kelas_map_pelajaran_id'],
	    		'semester_id'			=> $post['semester_id'],
	    		'siswa_id'				=> $post['siswa_id'][$key],
	    		'nilai'					=> $post['nilai'][$key],
	    		'keterangan'			=> $post['keterangan'][$key]
	    	);
	    	$result = $this->m_nilai->update($data_update);
	    }
	    if ($result) {
      		$this->session->set_flashdata('success', 'Data update success!');
	    } else {
      		$this->session->set_flashdata('error', 'Data update error!');
	    }
    	redirect($_SERVER['HTTP_REFERER']);
  	}
}
