<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelajaran_kelas extends MX_Controller {
	public function __construct()
  	{
    parent::__construct();
    $this->c_auth->checkLogin();
		$this->admin = $this->c_auth->getAdmin();
		$this->global = $this->c_global->data_list();
		$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
		$this->global_semester = $this->c_global->list_semester();
		$this->load->model('m_kelas_map_pelajaran');
		$this->load->model('m_kelas');
	}  
	
	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;
		$data['kelas'] = $this->m_kelas->get_kelas_by_kelas_map_id($_GET['kelas_map_id']);
		$data['kelas_map_pelajarans'] = $this->m_kelas_map_pelajaran->get_list_kelas_map_pelajaran($_GET['kelas_map_id']);
		$data['kelas_map_pelajaran'] = array();
	    foreach ($data['kelas_map_pelajarans'] as $key => $val) {
	      	$data['kelas_map_pelajaran'][$val['kelas_map_pelajaran_id']] = $val;
      		unset($val['kelas_map_pelajaran'][$key]);	
	    }
		$data['list_pelajaran'] = $this->m_kelas_map_pelajaran->get_list_pelajaran();
		$data['list_karyawan'] = $this->m_kelas_map_pelajaran->get_list_karyawan();
		$this->load->view('pelajaran_kelas',$data);
	}

	public function do_input(){
	    $post = $this->input->post();
	    $insert = array(
			'kelas_map_id'  => $post['kelas_map_id'],
			'pelajaran_id' 	=> $post['pelajaran_id'],
			'kkm' 	=> $post['kkm'],
			'karyawan_id' 	=> $post['karyawan_id']
	    );
	    $result_insert = $this->m_kelas_map_pelajaran->insert($insert);
	    if ($result_insert) {
	      	$this->session->set_flashdata('success', 'Data success insert!');
	    } else {
	        $this->session->set_flashdata('error', 'Data cannot insert!');
	    }
	    redirect(base_url().'pelajaran_kelas?kelas_map_id='.$post['kelas_map_id']);
  	}

  	public function do_update() {
	    $post = $this->input->post();
	    $id = $post['kelas_map_pelajaran_id'];
	    $data_update = array(
			'kelas_map_id'  => $post['kelas_map_id'],
			'pelajaran_id' 	=> $post['pelajaran_id'],
			'kkm' 	=> $post['kkm'],
			'karyawan_id' 	=> $post['karyawan_id']
	    );
	    $result_update = $this->m_kelas_map_pelajaran->update($id, $data_update);
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Data success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Data cannot updated!');
	  	}
	    redirect(base_url().'pelajaran_kelas?kelas_map_id='.$post['kelas_map_id']);
  	}

  	public function do_delete($id) {
	    $result_delete = $this->m_kelas_map_pelajaran->delete($id);
	    if ($result_delete) {
	      	$this->session->set_flashdata('success', 'Data success deleted!');
	  	} else {
		    $this->session->set_flashdata('error', 'Data cannot deleted!');
	  	}
	    // redirect(base_url().'pelajaran_kelas?kelas_map_id='.$post['kelas_map_id']);
	    redirect($_SERVER['HTTP_REFERER']);
  	}
}
