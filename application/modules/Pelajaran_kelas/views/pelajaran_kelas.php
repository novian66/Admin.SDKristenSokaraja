<!DOCTYPE html>
<html>

<head>

<?php include "template/head.php"; ?>

</head>

<body>
    <?php include "template/topbar.php"; ?>

    <div class="parent-wrapper toggled" id="outer-wrapper">

        <?php include "template/sidebar.php"; ?>

			<!-- MAIN CONTENT -->
			<div class="main-content" id="content-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12 clear-padding-xs">
							<h5 class="page-title">PELAJARAN KELAS <?php echo $kelas[0]['nama'] ?> <?php echo $admin['tahun_ajar'] ?></h5>
							<div class="section-divider"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 clear-padding-xs">
							<div class="col-sm-12">
								<div class="dash-item first-dash-item">
                                    <div class="inner-item">
                                    	<?php
	                                    $error = $this->session->flashdata('error');
	                                    if ($error) {
	                                        ?>
	                                        <div class="alert alert-danger alert-dismissable">
	                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
	                                            <?php echo $error; ?> <a href="#" class="alert-link">Error!</a>.
	                                        </div> 
	                                    <?php } ?>
	                                    <?php
	                                    $success = $this->session->flashdata('success');
	                                    if ($success) {
	                                        ?>
	                                        <div class="alert alert-success alert-dismissable">
	                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
	                                            <?php echo $success; ?> <a href="#" class="alert-link">Success!</a>.
	                                        </div> 
	                                    <?php } ?> 
	                                    <a class="btn btn-primary pull-right" onclick="tambahModal()">Tambah</a><br><br>
										<table id="attendenceDetailedTable" class="display responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>#</th>
													<th>Pelajaran</th>
													<th>KKM</th>
													<th>Guru</th>
													<th>Aksi</th>
												</tr>
											</thead>
											<tbody>
		                                        <?php
		                                        $no = 1;
		                                        foreach($kelas_map_pelajaran as $data) {
		                                        echo "<tr>
		                                                <td>".$no."</td>
		                                                <td>".$data['pelajaran_name']."</td>
		                                                <td>".$data['kkm']."</td>
		                                                <td>".$data['karyawan_name']."</td>
		                                                <td class='action-link'>
		                                                    <a class='edit' href='#' title='Edit' onclick='updateModal(".$data['kelas_map_pelajaran_id'].")'><i class='fa fa-edit'></i></a>
		                                                    <a class='delete' href='pelajaran_kelas/do_delete/".$data['kelas_map_pelajaran_id']."' title='Delete' onclick='return confirm()'><i class='fa fa-remove'></i></a>
		                                                </td>
		                                            </tr>";
		                                            $no++;
		                                        }; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="menu-togggle-btn">
					<a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
				</div>
				<div class="dash-footer col-lg-12">
					<p>SD Kristen Sokaraja</p>
				</div>
				
				<!-- Delete Modal -->
				<div id="deleteDetailModal" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><i class="fa fa-trash"></i>DELETE SECTION</h4>
							</div>
							<div class="modal-body">
								<div class="table-action-box">
									<a href="#" class="save"><i class="fa fa-check"></i>YES</a>
									<a href="#" class="cancel" data-dismiss="modal"><i class="fa fa-ban"></i>CLOSE</a>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				
				<!--Edit details modal-->
				<div id="editDetailModal" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><i class="fa fa-edit"></i>EDIT SECTION DETAILS</h4>
							</div>
							<div class="modal-body dash-form">
								<div class="col-sm-4">
									<label class="clear-top-margin"><i class="fa fa-book"></i>SECTION</label>
									<input type="text" placeholder="SECTION" value="A" />
								</div>
								<div class="col-sm-4">
									<label class="clear-top-margin"><i class="fa fa-code"></i>SECTION CODE</label>
									<input type="text" placeholder="SECTION CODE" value="PTH05A" />
								</div>
								<div class="col-sm-4">
									<label class="clear-top-margin"><i class="fa fa-user-secret"></i>SECTION CLASS</label>
									<select>
										<option>-- Select --</option>
										<option>5 STD</option>
										<option>6 STD</option>
									</select>
								</div>
								<div class="clearfix"></div>
								<div class="col-sm-12">
									<label><i class="fa fa-info-circle"></i>DESCRIPTION</label>
									<textarea placeholder="Enter Description Here"></textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="modal-footer">
								<div class="table-action-box">
									<a href="#" class="save"><i class="fa fa-check"></i>SAVE</a>
									<a href="#" class="cancel" data-dismiss="modal"><i class="fa fa-ban"></i>CLOSE</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div id="modalImage" class="modal fade" role="dialog">
				    <div class="modal-dialog modal-sm">
				        <!-- Modal content-->
				        <div class="modal-content">
				            <form action="pelajaran_kelas" method="post">
				                <div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal">&times;</button>
				                    <h4 class="modal-title"></i></h4>
				                </div>
				               	<input type="hidden" class="form-control" name="kelas_map_pelajaran_id" required="">
				               	<input type="hidden" class="form-control" name="kelas_map_id" required="">
				                <div class="modal-body">
				                    <div class="col-sm-12">
				                        <label class="col-form-label col-2">Pelajaran</label>
				                        <select class="form-control col-9" name="pelajaran_id" required="">
				                          <?php foreach ($list_pelajaran as $value): ?>
				                            <option value="<?php echo $value['pelajaran_id'] ?>"><?php echo $value['nama'] ?></option>
				                          <?php endforeach ?>
				                        </select>
				                    </div>
									<div class="col-sm-12">
				                        <label class="col-form-label col-2">KKM</label>
										<input type="text" class="form-control col-9" name="kkm" value="0" />
									</div>
				                    <div class="col-sm-12">
				                        <label class="col-form-label col-2">Guru</label>
				                        <select class="form-control col-9" name="karyawan_id" required="">
				                          <?php foreach ($list_karyawan as $value): ?>
				                            <option value="<?php echo $value['karyawan_id'] ?>"><?php echo $value['nama'] ?></option>
				                          <?php endforeach ?>
				                        </select>
				                    </div>
				                    
				                    <div class="clearfix"></div>
				                </div>
				                <div class="modal-footer">
				                    <div class="table-action-box">
				                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				                        <button type="submit" class="btn btn-primary">Simpan</button>
				                    </div>
				                </div>
				            </form>
				        </div>
				    </div>
				</div>
			</div>
		</div>

    <?php include "template/scripts.php"; ?>

</body>

</html>
<script type="text/javascript">
	function tambahModal() {
        $("#modalImage").modal('show');
        $("#modalImage").find('.modal-title').text('Tambah Data');
        $("#modalImage").find('form').find('input').val('');
        $("#modalImage").find('form').attr('action','pelajaran_kelas/do_input');
        $("input[name='kelas_map_id']").val('<?=$kelas[0]['kelas_map_id']?>');
    }

    function updateModal(kelas_map_pelajaran_id) {
        $("#modalImage").modal('show');
        $("#modalImage").find('.modal-title').text('Edit Data');
        $("#modalImage").find('form').find('input').val('');
        $("#modalImage").find('form').attr('action','pelajaran_kelas/do_update');
        var all = JSON.stringify(<?php echo json_encode($kelas_map_pelajaran) ?>);
        var obj = jQuery.parseJSON(all);
        $("input[name='kelas_map_pelajaran_id']").val(obj[kelas_map_pelajaran_id].kelas_map_pelajaran_id);
        $("input[name='kelas_map_id']").val(obj[kelas_map_pelajaran_id].kelas_map_id);
        $("input[name='kkm']").val(obj[kelas_map_pelajaran_id].kkm);
        $("select[name='pelajaran_id']").val(obj[kelas_map_pelajaran_id].pelajaran_id).attr('selected', 'selected');
        $("select[name='karyawan_id']").val(obj[kelas_map_pelajaran_id].karyawan_id).attr('selected', 'selected');
    }
</script>