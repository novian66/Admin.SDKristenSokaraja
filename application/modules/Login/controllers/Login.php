<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
  {
		parent::__construct();		
		$this->karyawan_id = $this->session->userdata('karyawan_id');
  } 
	public function index()
	{
		$data['ta'] = $this->db->get_where('tahun_ajar', array('status_id' => 13))->result_array();  
		$this->load->view('login', $data);
	}

	public function process(){

		$karyawan = $this->db->get_where('karyawan', array('nik' => $_POST['nik'], 'status_id' => 13))->result_array();  

		if(empty($karyawan)){

				$this->session->set_flashdata('message', array('condition'=>'warning','text'=>'Login gagal.'));
				$this->session->set_flashdata('content', $_POST);
				header("location: ".base_url()."login");	

				die();
		}

		else{        
			$password = $this->encryption->decrypt($karyawan[0]['password']);

			if($password==$_POST['password']){

				$this->session->set_userdata('karyawan_id',$karyawan[0]['karyawan_id']);
				$this->session->set_userdata('tahun_ajar_id',$_POST['tahun_ajar_id']);
				header("location: ".base_url()."profile_karyawan");	

				die();
			}

			else{
				$this->session->set_flashdata('message', array('condition'=>'warning','text'=>'Login gagal.'));
				$this->session->set_flashdata('content', $_POST);
				header("location: ".base_url()."login");

				die();
			}


		}

	}

	public function logout(){
		session_destroy();
		header("location: ".base_url()."welcome");
	}	
}
