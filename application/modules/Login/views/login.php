<!DOCTYPE html>
<html>

<head>

<?php include "template/head.php"; ?>

</head>

<body style="    padding: 80px 0 0;
    background-color: green;">

    <div style="margin:0 30% 0 30%;" class="" >

        <div class="col-lg-12 clear-padding-xs">
            <div class="col-sm-12">
                <div class="dash-item first-dash-item text-center">
                    <h6 class="item-title text-center">LOGIN<br>GURU/KARYAWAN</h6>
                    <?php
                    $message = $this->session->flashdata('message');
                    $content = $this->session->flashdata('content');
                    if ($message) {
                        ?>
                        <br>
                        <p class=""><?php echo $message['text'] ?></p>
                    <?php } ?> 
                    <div class="inner-item ">
                        <form method="post" action="login/process">
                        <div class="dash-form">
                            <label class="clear-top-margin">NIK</label>
                            <input type="text" name="nik" placeholder="NIK" required />
                            <label></i>PASSWORD</label>
                            <input type="password" name="password" required placeholder="Password" />
                            <label></i>TAHUN AJAR</label>
                            <select name="tahun_ajar_id" required>
                                <option value="">-- Pilih --</option>
                                <?php
                                    foreach($ta as $val){
                                        echo "<option value='".$val['tahun_ajar_id']."'>".$val['nama']."</option>";
                                    }
                                ?>
                            </select>
                            <br>                           
                            <br>                           
                            <input type="submit" class="btn btn-success" value="Masuk"/>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <?php include "template/scripts.php"; ?>

</body>

</html>