<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_karyawan extends MX_Controller {
	public function __construct()
  	{
    	parent::__construct();
    	$this->c_auth->checkLogin();
			$this->admin = $this->c_auth->getAdmin();
			$this->global = $this->c_global->data_list();
			$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
			$this->global_semester = $this->c_global->list_semester();
    	$this->load->model('m_karyawan');
  	}

  	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;		
		$data['title'] = 'Profil';
		$karyawan_id = $this->session->userdata('karyawan_id');
		$tahun_ajar_id = $this->session->userdata('tahun_ajar_id');
	    $data['karyawans'] = $this->m_karyawan->get_karyawan_by_id($karyawan_id, $tahun_ajar_id);
	    foreach ($data['karyawans'] as $key => $val) {
	      	$data['karyawan'][0] = $val;	
	    }
	    $this->load->view('profile_karyawan',$data);
	}

  	public function do_update() {
	    $post = $this->input->post();
	    $id = $this->session->userdata('karyawan_id');
	    $data_update = array(
			'email' 			=> $post['email'],
			'alamat' 			=> $post['alamat'],
			'no_telp' 			=> $post['no_telp']
	    );
	    $result_update = $this->m_karyawan->update($id, $data_update);
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Data success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Data cannot updated!');
	  	}
	    redirect(base_url().'profile_karyawan');
  	}

  	public function do_change_password() {
	    $post = $this->input->post();
	    $id = $this->session->userdata('karyawan_id');
	    if ($post['password'] == $post['password_konfirmasi']) {
		    $data_update = array(
				'password'	=> $this->encryption->encrypt($post['password'])
		    );
		    $result_update = $this->m_karyawan->update($id, $data_update);
	    } else {
	    	$result_update = false;
	    }
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Password success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Password cannot updated!');
	  	}
	    redirect(base_url().'profile_karyawan');
  	}

}