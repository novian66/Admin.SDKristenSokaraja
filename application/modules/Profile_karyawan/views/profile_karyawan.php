<!DOCTYPE html>
<html>

<head>

<?php include "template/head.php"; ?>

</head>

<body>
    <?php include "template/topbar.php"; ?>

    <div class="parent-wrapper toggled" id="outer-wrapper">

        <?php include "template/sidebar.php"; ?>

        <!-- MAIN CONTENT -->
        <div class="main-content" id="content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <h5 class="page-title"><i class="fa fa-users"></i><?php echo $title ?></h5>
                        <div class="section-divider"></div>       
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <div class="col-lg-12">
                            <div class="dash-item first-dash-item">
                                <div class="inner-item" style="overflow-x: auto;">
                                    <?php
                                    $error = $this->session->flashdata('error');
                                    if ($error) {
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <?php echo $error; ?> <a href="#" class="alert-link">Error!</a>.
                                        </div> 
                                    <?php } ?>
                                    <?php
                                    $success = $this->session->flashdata('success');
                                    if ($success) {
                                        ?>
                                        <div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <?php echo $success; ?> <a href="#" class="alert-link">Success!</a>.
                                        </div> 
                                    <?php } ?> 
                                    <div class="row">
                                            <a class="btn btn-danger pull-right" style="margin-left: 10px;" onclick="passwordModal(0)">Ubah Password</a>
                                            <a class="btn btn-primary pull-right" onclick="updateModal(0)">Edit Profil</a>
                                    </div>
                                    <br>
                                    <div class="row" style="border: 2px solid black; padding: 10px;">
                                        <div class="col-md-4">-</div>
                                        <div class="col-md-8">
                                            <div class="col-md-12">
                                                <label class="col-form-label col-md-4">Nama</label>
                                                <span>: <?=$karyawan[0]['nama']?></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="col-form-label col-md-4">Tanggal Lahir</label>
                                                <span>: <?=$karyawan[0]['tanggal_lahir']?></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="col-form-label col-md-4">Jenis Kelamin</label>
                                                <span>: <?=$karyawan[0]['jenis_kelamin_name']?></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="col-form-label col-md-4">NIP</label>
                                                <span>: <?=$karyawan[0]['nik']?></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="col-form-label col-md-4">Jabatan</label>
                                                <span>: <?=$karyawan[0]['jabatan_name']?></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="col-form-label col-md-4">Wali Kelas</label>
                                                <span>: <?=$karyawan[0]['kelas']?></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="col-form-label col-md-4">Mengajar</label>
                                                <span>: <?=$karyawan[0]['pelajaran']?></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="col-form-label col-md-4">Email</label>
                                                <span>: <?=$karyawan[0]['email']?></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="col-form-label col-md-4">No Telp</label>
                                                <span>: <?=$karyawan[0]['no_telp']?></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="col-form-label col-md-4">Alamat</label>
                                                <span>: <?=$karyawan[0]['alamat']?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-togggle-btn">
                <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
            </div>
            <div class="dash-footer col-lg-12">
                <p>SD Kristen Sokaraja</p>
            </div>
        </div>
    </div>
    <?php include "template/scripts.php"; ?>
</body>
</html>
<!--Kelas karyawan-->
<div id="modalImage" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="karyawan_kelas" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></i></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Email</label>
                            <input type="email" class="form-control col-9" name="email" required="">
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Telp</label>
                            <input type="text" class="form-control col-9" name="no_telp" required="">
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Alamat</label>
                            <input type="text" class="form-control col-9" name="alamat" required="">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <div class="table-action-box">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="modalPassword" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="karyawan_kelas" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></i></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Password</label>
                            <input type="password" class="form-control col-9" name="password" required="" onkeyup="validPassword()">
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Password Konfirmasi</label>
                            <input type="password" class="form-control col-9" name="password_konfirmasi" required="" onkeyup="validPassword()">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <div class="table-action-box">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary" id="submit_password" disabled="true">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function updateModal(karyawan_id) {
        $("#modalImage").modal('show');
        $("#modalImage").find('.modal-title').text('Edit Data');
        $("#modalImage").find('form').find('input').val('');
        $("#modalImage").find('form').attr('action','profile_karyawan/do_update');
        var all = JSON.stringify(<?php echo json_encode($karyawan) ?>);
        var obj = jQuery.parseJSON(all);
        $("input[name='email']").val(obj[karyawan_id].email);
        $("input[name='no_telp']").val(obj[karyawan_id].no_telp);
        $("input[name='alamat']").val(obj[karyawan_id].alamat);
    }

    function passwordModal() {
        $("#modalPassword").modal('show');
        $("#modalPassword").find('.modal-title').text('Ubah Password');
        $("#modalPassword").find('form').find('input').val('');
        $("#modalPassword").find('form').attr('action','profile_karyawan/do_change_password');
    }

    function validPassword() {
        var password = $("#modalPassword").find('input[name="password"]').val();
        var password_konfirmasi = $("#modalPassword").find('input[name="password_konfirmasi"]').val();
        if (password != password_konfirmasi) {
            $('#submit_password').prop('disabled', true);
        } else {
            $('#submit_password').removeAttr('disabled');
        }
    }
</script>