<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelajaran extends MX_Controller {
	public function __construct()
  	{
    	parent::__construct();
    	$this->c_auth->checkLogin();
		$this->admin = $this->c_auth->getAdmin();
		$this->global = $this->c_global->data_list();
		$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
		$this->global_semester = $this->c_global->list_semester();
    	$this->load->model('m_pelajaran');
  	}

  	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;
		$data['title'] = 'Master Pelajaran';
	    $data['pelajarans'] = $this->m_pelajaran->data_pelajaran();
	    foreach ($data['pelajarans'] as $key => $val) {
	      	$data['pelajaran'][$val['pelajaran_id']] = $val;
	    }
      	unset($data['pelajaran'][0]);
	    $data['list_is_active'] = $this->m_pelajaran->get_list_is_active();
		$this->load->view('pelajaran',$data);
	}

	public function do_input(){
	    $post = $this->input->post();
	    $insert = array(
			'nama'         	=> $post['nama'],
			'status_id' 	=> $post['status_id']
	    );
	    $result_insert = $this->m_pelajaran->insert($insert);
	    if ($result_insert) {
	      	$this->session->set_flashdata('success', 'Data success insert!');
	    } else {
	        $this->session->set_flashdata('error', 'Data cannot insert!');
	    }
	    redirect(base_url().'pelajaran');
  	}

  	public function do_update() {
	    $post = $this->input->post();
	    $id = $post['pelajaran_id'];
	    $data_update = array(
			'nama'         	=> $post['nama'],
			'status_id' 	=> $post['status_id']
	    );
	    $result_update = $this->m_pelajaran->update($id, $data_update);
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Data success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Data cannot updated!');
	  	}
	    redirect(base_url().'pelajaran');
  	}

  	public function do_delete($id) {
	    $result_delete = $this->m_pelajaran->delete($id);
	    if ($result_delete) {
	      	$this->session->set_flashdata('success', 'Data success deleted!');
	  	} else {
		    $this->session->set_flashdata('error', 'Data cannot deleted!');
	  	}
	    redirect(base_url().'pelajaran');
  	}
}
