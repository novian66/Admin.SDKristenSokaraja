<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {
	public function __construct()
  {
    parent::__construct();
    $this->c_auth->checkLogin();
		$this->admin = $this->c_auth->getAdmin();
		$this->global = $this->c_global->data_list();
		$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
		$this->global_semester = $this->c_global->list_semester();
  }  
	public function index()
	{
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;		
		$this->load->view('welcome',$data);
	}
}
