<!DOCTYPE html>
<html>

<head>

<?php include "template/head.php"; ?>

</head>

<body>
    <?php include "template/topbar.php"; ?>

    <div class="parent-wrapper toggled" id="outer-wrapper">

        <?php include "template/sidebar.php"; ?>

        <!-- MAIN CONTENT -->
        <div class="main-content" id="content-wrapper">
        
        </div>
    </div>

    <?php include "template/modal.php"; ?>
    <?php include "template/scripts.php"; ?>

</body>

</html>