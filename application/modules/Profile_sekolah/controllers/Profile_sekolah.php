<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_sekolah extends MX_Controller {
	public function __construct()
  	{
    	parent::__construct();
    	$this->c_auth->checkLogin();
			$this->admin = $this->c_auth->getAdmin();
			$this->global = $this->c_global->data_list();
			$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
			$this->global_semester = $this->c_global->list_semester();
			$this->load->model('m_profile_sekolah');
			$this->load->model('m_prestasi');  	
  	}

  	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;		
		$data['title'] = 'Profil Sekolah';
	    $data['profile_sekolahs'] = $this->m_profile_sekolah->data_profile_sekolah();
	    foreach ($data['profile_sekolahs'] as $key => $val) {
	      	$data['profile_sekolah'][$val['profil_id']] = $val;
      		unset($val['profile_sekolah'][$key]);	
			}
			$data['prestasis'] = $this->m_prestasi->data_prestasi();
			$data['prestasi'] = array();
	    foreach ($data['prestasis'] as $key => $val) {
	      	$data['prestasi'][$val['prestasi_id']] = $val;
      		unset($val['prestasi'][$key]);	
	    }
	    $data['list_is_active'] = $this->m_prestasi->get_list_is_active();
		$this->load->view('profile_sekolah',$data);
	}

  	public function do_update_text() {
	    $post = $this->input->post();
	    foreach ($post['profil_id'] as $key => $value) {
	    	$id = $value;
		    $data_update = array(
				'isi'	=> $post['isi'][$key]
		    );
		    $result_update = $this->m_profile_sekolah->update($id, $data_update);
	    }
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Data success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Data cannot updated!');
	  	}
	    redirect(base_url().'profile_sekolah');
		}
		
		public function do_input(){
	    $post = $this->input->post();
	    $insert = array(
			'nama'         	=> $post['nama'],
			'deskripsi'         	=> $post['deskripsi'],
			'status_id' 	=> $post['status_id']
	    );
	    $result_insert = $this->m_prestasi->insert($insert);
	    if ($result_insert) {
	      	$this->session->set_flashdata('success', 'Data success insert!');
	    } else {
	        $this->session->set_flashdata('error', 'Data cannot insert!');
	    }
	    redirect(base_url().'Profile_sekolah#prestasi');
  	}

  	public function do_update() {
	    $post = $this->input->post();
	    $id = $post['prestasi_id'];
	    $data_update = array(
			'nama'         	=> $post['nama'],
			'deskripsi'         	=> $post['deskripsi'],
			'status_id' 	=> $post['status_id']
	    );
	    $result_update = $this->m_prestasi->update($id, $data_update);
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Data success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Data cannot updated!');
	  	}
	    redirect(base_url().'Profile_sekolah#prestasi');
  	}

  	public function do_delete($id) {
	    $result_delete = $this->m_prestasi->delete($id);
	    if ($result_delete) {
	      	$this->session->set_flashdata('success', 'Data success deleted!');
	  	} else {
		    $this->session->set_flashdata('error', 'Data cannot deleted!');
	  	}
	    redirect(base_url().'Profile_sekolah#prestasi');
  	}
}
