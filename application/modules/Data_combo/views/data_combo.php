<!DOCTYPE html>
<html>

<head>

<?php include "template/head.php"; ?>

</head>

<body>
    <?php include "template/topbar.php"; ?>

    <div class="parent-wrapper toggled" id="outer-wrapper">

        <?php include "template/sidebar.php"; ?>

        <!-- MAIN CONTENT -->
        <div class="main-content" id="content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <h5 class="page-title"><i class="fa fa-users"></i><?php echo $title ?></h5>
                        <div class="section-divider"></div>                    
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <div class="col-lg-12">
                            <div class="dash-item first-dash-item">
                                <div class="inner-item">
                                    <?php
                                    $error = $this->session->flashdata('error');
                                    if ($error) {
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <?php echo $error; ?> <a href="#" class="alert-link">Error!</a>.
                                        </div> 
                                    <?php } ?>
                                    <?php
                                    $success = $this->session->flashdata('success');
                                    if ($success) {
                                        ?>
                                        <div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <?php echo $success; ?> <a href="#" class="alert-link">Success!</a>.
                                        </div> 
                                    <?php } ?> 
                                    <a class="btn btn-primary pull-right" onclick="tambahModal()">Tambah</a><br><br>
                                    <table id="attendenceDetailedTable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kode</th>
                                                <th>Nama</th>
                                                <th>Group</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        foreach($data_combo as $data) {
                                        echo "<tr>
                                                <td>".$no."</td>
                                                <td>".$data['kode']."</td>
                                                <td>".$data['nama']."</td>
                                                <td>".$data['group_list']."</td>
                                                <td class='action-link'>
                                                    <a class='edit' href='#' title='Edit' onclick='updateModal(".$data['data_combo_id'].")'><i class='fa fa-edit'></i></a>
                                                    <a class='delete' href='data_combo/do_delete/".$data['data_combo_id']."' title='Delete' onclick='return confirm()'><i class='fa fa-remove'></i></a>
                                                </td>
                                            </tr>";
                                            $no++;
                                        }; ?>
                                        </tbody>
                                        </table>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-togggle-btn">
                <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
            </div>
            <div class="dash-footer col-lg-12">
                <p>SD Kristen Sokaraja</p>
            </div>
        </div>
    </div>
    <?php include "template/scripts.php"; ?>
</body>
</html>
<!--Kelas group_list-->
<div id="modalImage" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="group_list_kelas" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></i></h4>
                </div>
                    <input type="hidden" class="form-control" name="data_combo_id" required="">
                <div class="modal-body">
                    <div class="col-sm-12">
                        <label class="col-form-label col-2">Kode</label>
                        <input type="text" class="form-control col-9" name="kode" required="">
                    </div>
                    <div class="col-sm-12">
                        <label class="col-form-label col-2">Nama</label>
                        <input type="text" class="form-control col-9" name="nama" required="">
                    </div>
                    <div class="col-sm-12">
                        <label class="col-form-label col-2">Group</label>
                        <select class="form-control col-9" name="group_list" required="">
                            <option value="jk">JK</option>
                            <option value="karyawan">Karyawan</option>
                            <option value="semester">Semester</option>
                            <option value="status">Status</option>
                            <option value="agama">Agama</option>
                            <option value="hari">Hari</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <div class="table-action-box">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#mytable").DataTable();
    });

    function tambahModal() {
        $("#modalImage").modal('show');
        $("#modalImage").find('.modal-title').text('Tambah Data');
        $("#modalImage").find('form').find('input').val('');
        $("#modalImage").find('form').attr('action','data_combo/do_input');
    }

    function updateModal(data_combo_id) {
        $("#modalImage").modal('show');
        $("#modalImage").find('.modal-title').text('Edit Data');
        $("#modalImage").find('form').find('input').val('');
        $("#modalImage").find('form').attr('action','data_combo/do_update');
        var all = JSON.stringify(<?php echo json_encode($data_combo) ?>);
        var obj = jQuery.parseJSON(all);
        console.log(obj);
        $("input[name='data_combo_id']").val(obj[data_combo_id].data_combo_id);
        $("input[name='kode']").val(obj[data_combo_id].kode);
        $("input[name='nama']").val(obj[data_combo_id].nama);
        $("select[name='group_list']").val(obj[data_combo_id].group_list).attr('selected', 'selected');
    }
</script>