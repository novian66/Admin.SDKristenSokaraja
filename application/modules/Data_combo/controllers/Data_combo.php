<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_combo extends MX_Controller {
	public function __construct()
  	{
    	parent::__construct();
    	$this->c_auth->checkLogin();
			$this->admin = $this->c_auth->getAdmin();
			$this->global = $this->c_global->data_list();
			$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
			$this->global_semester = $this->c_global->list_semester();
    	$this->load->model('m_data_combo');
  	}

  	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;		
		$data['title'] = 'Master Data List';
	    $data['data_combos'] = $this->m_data_combo->data_data_combo();
	    $data['data_combo'] = array();
	    foreach ($data['data_combos'] as $key => $val) {
      		$data['data_combo'][$val['data_combo_id']] = $val;
      		unset($val['data_combo'][$key]);	
	    }
      	$this->load->view('data_combo',$data);
	}

	public function do_input(){
	    $post = $this->input->post();
	    $insert = array(
			'kode'         	=> $post['kode'],
			'nama'         	=> $post['nama'],
			'group_list' 	=> $post['group_list']
	    );
	    $result_insert = $this->m_data_combo->insert($insert);
	    if ($result_insert) {
	      	$this->session->set_flashdata('success', 'Data success insert!');
	    } else {
	        $this->session->set_flashdata('error', 'Data cannot insert!');
	    }
	    redirect(base_url().'data_combo');
  	}

  	public function do_update() {
	    $post = $this->input->post();
	    $id = $post['data_combo_id'];
	    $data_update = array(
			'kode'         	=> $post['kode'],
			'nama'         	=> $post['nama'],
			'group_list' 	=> $post['group_list']
	    );
	    $result_update = $this->m_data_combo->update($id, $data_update);
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Data success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Data cannot updated!');
	  	}
	    redirect(base_url().'data_combo');
  	}

  	public function do_delete($id) {
	    $result_delete = $this->m_data_combo->delete($id);
	    if ($result_delete) {
	      	$this->session->set_flashdata('success', 'Data success deleted!');
	  	} else {
		    $this->session->set_flashdata('error', 'Data cannot deleted!');
	  	}
	    redirect(base_url().'data_combo');
  	}
}
