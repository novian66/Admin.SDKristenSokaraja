<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class siswa extends MX_Controller {
	public function __construct()
  	{
    	parent::__construct();
    	$this->c_auth->checkLogin();
		$this->admin = $this->c_auth->getAdmin();
		$this->global = $this->c_global->data_list();
		$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
		$this->global_semester = $this->c_global->list_semester();
    	$this->load->model('m_siswa');
  	}

  	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;
		$data['title'] = 'Master siswa';
	    $data['siswas'] = $this->m_siswa->data_siswa();
	    foreach ($data['siswas'] as $key => $val) {
	      	$data['siswa'][$val['siswa_id']] = $val;
      		unset($val['siswa'][$key]);	
	    }
	    $data['list_is_active'] = $this->m_siswa->get_list_is_active();
	    $data['list_agama'] = $this->m_siswa->get_list_agama();
	    $data['list_jenis_kelamin'] = $this->m_siswa->get_list_jenis_kelamin();
		$this->load->view('siswa',$data);
	}

	public function do_input(){
	    $post = $this->input->post();
	    $insert = array(
			'nis' 				=> $post['nis'],
			'nama'         		=> $post['nama'],
			'tanggal_lahir' 	=> $post['tanggal_lahir'],
			'jenis_kelamin_id' 	=> $post['jenis_kelamin_id'],
			'agama_id' 			=> $post['agama_id'],
			'nama_ortu' 		=> $post['nama_ortu'],
			'email_ortu' 		=> $post['email_ortu'],
			'no_telp' 			=> $post['no_telp'],
			'telegram' 			=> $post['telegram'],
			'tanggal_masuk' 	=> $post['tanggal_masuk'],
			'alamat' 			=> $post['alamat'],
			'status_id' 		=> $post['status_id'],
			'password'			=> $this->encryption->encrypt($post['password'])
	    );

	    $result_insert = $this->m_siswa->insert($insert);
	    if ($result_insert) {
	      	$this->session->set_flashdata('success', 'Data success insert!');
	    } else {
	        $this->session->set_flashdata('error', 'Data cannot insert!');
	    }
	    redirect(base_url().'siswa');
  	}

  	public function do_update() {
	    $post = $this->input->post();
	    $id = $post['siswa_id'];
	    $data_update = array(
			'nis' 				=> $post['nis'],
			'nama'         		=> $post['nama'],
			'tanggal_lahir' 	=> $post['tanggal_lahir'],
			'jenis_kelamin_id' 	=> $post['jenis_kelamin_id'],
			'agama_id' 			=> $post['agama_id'],
			'nama_ortu' 		=> $post['nama_ortu'],
			'email_ortu' 		=> $post['email_ortu'],
			'no_telp' 			=> $post['no_telp'],
			'telegram' 			=> $post['telegram'],
			'tanggal_masuk' 	=> $post['tanggal_masuk'],
			'alamat' 			=> $post['alamat'],
			'status_id' 		=> $post['status_id']
	    );

	    if (isset($post['password'])) {
	    	$data_update['password'] = $this->encryption->encrypt($post['password']);
	    }
	    $result_update = $this->m_siswa->update($id, $data_update);
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Data success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Data cannot updated!');
	  	}
	    redirect(base_url().'siswa');
  	}

  	public function do_delete($id) {
	    $result_delete = $this->m_siswa->delete($id);
	    if ($result_delete) {
	      	$this->session->set_flashdata('success', 'Data success deleted!');
	  	} else {
		    $this->session->set_flashdata('error', 'Data cannot deleted!');
	  	}
	    redirect(base_url().'siswa');
  	}
}
