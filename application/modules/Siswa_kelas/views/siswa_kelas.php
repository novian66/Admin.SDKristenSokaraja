<!DOCTYPE html>
<html>

<head>

<?php include "template/head.php"; ?>

</head>

<body>
    <?php include "template/topbar.php"; ?>

    <div class="parent-wrapper toggled" id="outer-wrapper">

        <?php include "template/sidebar.php"; ?>

			<!-- MAIN CONTENT -->
			<div class="main-content" id="content-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12 clear-padding-xs">
							<h5 class="page-title">SISWA KELAS <?php echo $kelas[0]['nama'] ?> <?php echo $admin['tahun_ajar'] ?></h5>
							<div class="section-divider"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 clear-padding-xs">
							<div class="col-sm-12">
								<div class="dash-item first-dash-item">
                                    <div class="inner-item">
                                    	<?php
	                                    $error = $this->session->flashdata('error');
	                                    if ($error) {
	                                        ?>
	                                        <div class="alert alert-danger alert-dismissable">
	                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
	                                            <?php echo $error; ?> <a href="#" class="alert-link">Error!</a>.
	                                        </div> 
	                                    <?php } ?>
	                                    <?php
	                                    $success = $this->session->flashdata('success');
	                                    if ($success) {
	                                        ?>
	                                        <div class="alert alert-success alert-dismissable">
	                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
	                                            <?php echo $success; ?> <a href="#" class="alert-link">Success!</a>.
	                                        </div> 
	                                    <?php } ?> 
	                                    <form action="siswa_kelas/do_update" method="post">
	                                    	<input type="hidden" name="tahun_ajar_id" value="<?=$kelas[0]['tahun_ajar_id']?>">
	                                    	<input type="hidden" name="kelas_id" value="<?=$kelas[0]['kelas_id']?>">
	                                    	<input type="hidden" name="kelas_map_id" value="<?=$kelas_map_id?>">
											<table id="attendenceDetailedTable" class="display responsive nowrap" cellspacing="0" width="100%">
												<thead>
													<tr>
														<th>#</th>
														<th>NIS</th>
														<th>Nama Siswa</th>
														<th>Aksi</th>
													</tr>
												</thead>
												<tbody>
			                                        <?php
			                                        $no = 1;
			                                        foreach($siswa_kelas as $data) {
			                                        echo "<tr>
			                                                <td>".$no."</td>
			                                                <td>".$data['siswa_id']."</td>
			                                                <td>".$data['nama']."</td>
			                                                <td class='action-link'>
			                                                    <input type='checkbox' name='siswa_id[".$data['siswa_id']."]' ".$data['stat']." />
			                                                </td>
			                                            </tr>";
			                                            $no++;
			                                        }; ?>
												</tbody>
											</table>
		                                    <div class="text-center">
		                                    	<input class="btn btn-primary" type="submit" value="Simpan" />
		                                    </div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="menu-togggle-btn">
					<a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
				</div>
				<div class="dash-footer col-lg-12">
					<p>SD Kristen Sokaraja</p>
				</div>
			
			</div>
		</div>

    <?php include "template/scripts.php"; ?>

</body>

</html>