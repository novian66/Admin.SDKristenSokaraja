<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa_kelas extends MX_Controller {
	public function __construct()
  	{
	    parent::__construct();
	    $this->c_auth->checkLogin();
		$this->admin = $this->c_auth->getAdmin();
		$this->global = $this->c_global->data_list();
		$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
		$this->global_semester = $this->c_global->list_semester();
		$this->load->model('m_siswa_kelas');
		$this->load->model('m_kelas');
	}  
	
	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;
		$data['kelas_map_id'] = $_GET['kelas_map_id'];
		$data['kelas'] = $this->m_kelas->get_kelas_by_kelas_map_id($_GET['kelas_map_id']);
		$data['siswa_kelas'] = $this->m_siswa_kelas->get_list_siswa_kelas($data['kelas'][0]['tahun_ajar_id'], $data['kelas'][0]['kelas_id']);
		$this->load->view('siswa_kelas',$data);
	}

  	public function do_update() {
	    $post = $this->input->post();
	    $siswa_kelas_old = array();
	    $siswa_checked = array();
		$data['siswa_kelas'] = $this->m_siswa_kelas->get_list_siswa_kelas($post['tahun_ajar_id'], $post['kelas_id']);
		foreach ($data['siswa_kelas'] as $key => $value) {
			if ($value['stat'] == 'checked') {
				array_push($siswa_kelas_old, $value['siswa_id']);
			}
		}
	    foreach ($post['siswa_id'] as $key => $value) {
			array_push($siswa_checked, $key);
	    	if (in_array($key, $siswa_kelas_old)) {
				null;
			} else {
				$data_insert = array(
					'kelas_map_id' 	=> $post['kelas_map_id'],
					'siswa_id'		=> $key	
				);
				$result = $this->m_siswa_kelas->insert($data_insert);
			}
	    }
	    foreach ($data['siswa_kelas'] as $key => $value) {
			if ($value['stat'] == 'checked') {
				if (in_array($value['siswa_id'], $siswa_checked)) {
					null;
				} else {
					$data_delete = array(
						'kelas_map_id' 	=> $post['kelas_map_id'],
						'siswa_id'		=> $value['siswa_id']
					);
					$result = $this->m_siswa_kelas->delete($data_delete);
				}
			}
		}
      	$this->session->set_flashdata('success', 'Data update success!');
    	redirect($_SERVER['HTTP_REFERER']);
  	}
}
