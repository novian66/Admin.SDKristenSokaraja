<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends MX_Controller {
	public function __construct()
  	{
			parent::__construct();
			$this->c_auth->checkLogin();
			$this->admin = $this->c_auth->getAdmin();
			$this->global = $this->c_global->data_list();
			$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
			$this->global_semester = $this->c_global->list_semester();
    	$this->load->model('m_galeri');
  	}

  	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;		
		$data['title'] = 'Galeri';
	    $data['galeris'] = $this->m_galeri->data_galeri();
	    $data['galeri'] = array();
	    foreach ($data['galeris'] as $key => $val) {
      		$data['galeri'][$val['galeri_id']] = $val;
      		unset($val['galeri'][$key]);	
	    }
      	$this->load->view('galeri',$data);
	}

	public function do_input(){
	    $post = $this->input->post();
	    if($_FILES['gambar']['name'] != NULL) {
			$config['upload_path']          = './assets/galeri/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['encrypt_name']			= TRUE;
			$config['max_size']             = 2048;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('gambar')) {
				$error = $this->upload->display_errors();
	        	$this->session->set_flashdata('error', 'Data cannot upload!'.$error);
	    		redirect(base_url().'galeri');
			} else {
				$dataupload = $this->upload->data();
			}
		}
	    $insert = array(
			'nama'         	=> $post['nama'],
			'deskripsi' 	=> $post['deskripsi'],
			'gambar'		=> $dataupload['file_name']
	    );
	    $result_insert = $this->m_galeri->insert($insert);
	    if ($result_insert) {
	      	$this->session->set_flashdata('success', 'Data success insert!');
	    } else {
	        $this->session->set_flashdata('error', 'Data cannot insert!');
	    }
	    redirect(base_url().'galeri');
  	}

  	public function do_update() {
	    $post = $this->input->post();
	    $id = $post['galeri_id'];
	    $data_update = array(
			'nama'         	=> $post['nama'],
			'deskripsi' 	=> $post['deskripsi']
	    );
     	if($_FILES['gambar']['name'] != NULL) {
			$config['upload_path']          = './assets/galeri/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['encrypt_name']			= TRUE;
			$config['max_size']             = 2048;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('gambar')) {
				$error = $this->upload->display_errors();
	        	$this->session->set_flashdata('error', 'Data cannot upload!'.$error);
	    		redirect(base_url().'galeri');
			} else {
				$dataupload = $this->upload->data();
				$data_update['gambar'] = $dataupload['file_name'];
			}
		}
	    $result_update = $this->m_galeri->update($id, $data_update);
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Data success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Data cannot updated!');
	  	}
	    redirect(base_url().'galeri');
  	}

  	public function do_delete($id) {
	    $result_delete = $this->m_galeri->delete($id);
	    if ($result_delete) {
	      	$this->session->set_flashdata('success', 'Data success deleted!');
	  	} else {
		    $this->session->set_flashdata('error', 'Data cannot deleted!');
	  	}
	    redirect(base_url().'galeri');
  	}
}
