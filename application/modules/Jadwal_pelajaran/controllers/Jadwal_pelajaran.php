<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_pelajaran extends MX_Controller {
	public function __construct()
  	{
    parent::__construct();
    $this->c_auth->checkLogin();
		$this->admin = $this->c_auth->getAdmin();
		$this->global = $this->c_global->data_list();
		$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
		$this->global_semester = $this->c_global->list_semester();
		$this->load->model('m_jadwal_pelajaran');
		$this->load->model('m_kelas');
	}  
	
	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;		
		$data['kelas'] = $this->m_kelas->get_kelas_by_kelas_map_id($_GET['kelas_map_id']);
		$data['jadwal_pelajarans'] = $this->m_jadwal_pelajaran->get_jadwal_pelajaran($_GET['kelas_map_id']);
		$data['jadwal_pelajaran'] = array();
	    foreach ($data['jadwal_pelajarans'] as $key => $val) {
	      	$data['jadwal_pelajaran'][$val['jadwal_pelajaran_id']] = $val;
      		unset($val['jadwal_pelajaran'][$key]);	
	    }
		$data['list_pelajaran_karyawan'] = $this->m_jadwal_pelajaran->get_list_kelas_map_pelajaran($_GET['kelas_map_id']);
		$data['list_hari'] = $this->m_jadwal_pelajaran->get_list_hari();
		$this->load->view('jadwal_pelajaran',$data);
	}

	public function do_input(){
	    $post = $this->input->post();
	    $insert = array(
			'kelas_map_pelajaran_id'=> $post['kelas_map_pelajaran_id'],
			'hari_id' 				=> $post['hari_id'],
			'jam'					=> $post['jam']
	    );
	    $result_insert = $this->m_jadwal_pelajaran->insert($insert);
	    if ($result_insert) {
	      	$this->session->set_flashdata('success', 'Data success insert!');
	    } else {
	        $this->session->set_flashdata('error', 'Data cannot insert!');
	    }
	    redirect($_SERVER['HTTP_REFERER']);
  	}

  	public function do_update() {
	    $post = $this->input->post();
	    $id = $post['jadwal_pelajaran_id'];
	    $data_update = array(
			'kelas_map_pelajaran_id'=> $post['kelas_map_pelajaran_id'],
			'hari_id' 				=> $post['hari_id'],
			'jam'					=> $post['jam']
	    );
	    $result_update = $this->m_jadwal_pelajaran->update($id, $data_update);
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Data success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Data cannot updated!');
	  	}
	    redirect($_SERVER['HTTP_REFERER']);
  	}

  	public function do_delete($id) {
	    $result_delete = $this->m_jadwal_pelajaran->delete($id);
	    if ($result_delete) {
	      	$this->session->set_flashdata('success', 'Data success deleted!');
	  	} else {
		    $this->session->set_flashdata('error', 'Data cannot deleted!');
	  	}
	    redirect($_SERVER['HTTP_REFERER']);
  	}
}
