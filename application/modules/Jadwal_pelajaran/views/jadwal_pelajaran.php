<!DOCTYPE html>
<html>

<head>

<?php include "template/head.php"; ?>

</head>

<body>
    <?php include "template/topbar.php"; ?>

    <div class="parent-wrapper toggled" id="outer-wrapper">

        <?php include "template/sidebar.php"; ?>

			<!-- MAIN CONTENT -->
			<div class="main-content" id="content-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12 clear-padding-xs">
							<h5 class="page-title">JADWAL KELAS <?php echo $kelas[0]['nama'] ?> <?php echo $admin['tahun_ajar'] ?></h5>
							<div class="section-divider"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 clear-padding-xs">
							<div class="col-sm-12">
								<div class="dash-item first-dash-item">
                                    <div class="inner-item">
                                    	<?php
	                                    $error = $this->session->flashdata('error');
	                                    if ($error) {
	                                        ?>
	                                        <div class="alert alert-danger alert-dismissable">
	                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
	                                            <?php echo $error; ?> <a href="#" class="alert-link">Error!</a>.
	                                        </div> 
	                                    <?php } ?>
	                                    <?php
	                                    $success = $this->session->flashdata('success');
	                                    if ($success) {
	                                        ?>
	                                        <div class="alert alert-success alert-dismissable">
	                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
	                                            <?php echo $success; ?> <a href="#" class="alert-link">Success!</a>.
	                                        </div> 
	                                    <?php } ?> 
	                                    <a class="btn btn-primary pull-right" onclick="tambahModal()">Tambah</a><br><br>
										<table id="attendenceDetailedTable" class="display responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>#</th>
													<th>Pelajaran</th>
													<th>Guru</th>
													<th>Hari</th>
													<th>Jadwal</th>
													<th>Aksi</th>
												</tr>
											</thead>
											<tbody>
		                                        <?php
		                                        $no = 1;
		                                        foreach($jadwal_pelajaran as $data) {
		                                        echo "<tr>
		                                                <td>".$no."</td>
		                                                <td>".$data['pelajaran_name']."</td>
		                                                <td>".$data['karyawan_name']."</td>
		                                                <td>".$data['hari_name']."</td>
		                                                <td>".$data['jam']."</td>
		                                                <td class='action-link'>
		                                                    <a class='edit' href='#' title='Edit' onclick='updateModal(".$data['jadwal_pelajaran_id'].")'><i class='fa fa-edit'></i></a>
		                                                    <a class='delete' href='jadwal_pelajaran/do_delete/".$data['jadwal_pelajaran_id']."' title='Delete' onclick='return confirm()'><i class='fa fa-remove'></i></a>
		                                                </td>
		                                            </tr>";
		                                            $no++;
		                                        }; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="menu-togggle-btn">
					<a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
				</div>
				<div class="dash-footer col-lg-12">
					<p>SD Kristen Sokaraja</p>
				</div>
				
				<!-- Delete Modal -->
				<div id="deleteDetailModal" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><i class="fa fa-trash"></i>DELETE SECTION</h4>
							</div>
							<div class="modal-body">
								<div class="table-action-box">
									<a href="#" class="save"><i class="fa fa-check"></i>YES</a>
									<a href="#" class="cancel" data-dismiss="modal"><i class="fa fa-ban"></i>CLOSE</a>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				
				<!--Edit details modal-->
				<div id="editDetailModal" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><i class="fa fa-edit"></i>EDIT SECTION DETAILS</h4>
							</div>
							<div class="modal-body dash-form">
								<div class="col-sm-4">
									<label class="clear-top-margin"><i class="fa fa-book"></i>SECTION</label>
									<input type="text" placeholder="SECTION" value="A" />
								</div>
								<div class="col-sm-4">
									<label class="clear-top-margin"><i class="fa fa-code"></i>SECTION CODE</label>
									<input type="text" placeholder="SECTION CODE" value="PTH05A" />
								</div>
								<div class="col-sm-4">
									<label class="clear-top-margin"><i class="fa fa-user-secret"></i>SECTION CLASS</label>
									<select>
										<option>-- Select --</option>
										<option>5 STD</option>
										<option>6 STD</option>
									</select>
								</div>
								<div class="clearfix"></div>
								<div class="col-sm-12">
									<label><i class="fa fa-info-circle"></i>DESCRIPTION</label>
									<textarea placeholder="Enter Description Here"></textarea>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="modal-footer">
								<div class="table-action-box">
									<a href="#" class="save"><i class="fa fa-check"></i>SAVE</a>
									<a href="#" class="cancel" data-dismiss="modal"><i class="fa fa-ban"></i>CLOSE</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div id="modalImage" class="modal fade" role="dialog">
				    <div class="modal-dialog modal-sm">
				        <!-- Modal content-->
				        <div class="modal-content">
				            <form action="pelajaran_kelas" method="post">
				            	<input type="hidden" name="jadwal_pelajaran_id">
				                <div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal">&times;</button>
				                    <h4 class="modal-title"></i></h4>
				                </div>
				                <div class="modal-body">
				                    <div class="col-sm-12">
				                        <label class="col-form-label col-2">Pelajaran</label>
				                        <select class="form-control col-9" name="kelas_map_pelajaran_id" required="">
				                          <?php foreach ($list_pelajaran_karyawan as $value): ?>
				                            <option value="<?php echo $value['kelas_map_pelajaran_id'] ?>"><?php echo $value['pelajaran_name'] ?> - <?php echo $value['karyawan_name'] ?></option>
				                          <?php endforeach ?>
				                        </select>
				                    </div>
				                    <div class="col-sm-12">
				                        <label class="col-form-label col-2">Hari</label>
				                        <select class="form-control col-9" name="hari_id" required="">
				                          <?php foreach ($list_hari as $value): ?>
				                            <option value="<?php echo $value['data_combo_id'] ?>"><?php echo $value['nama'] ?></option>
				                          <?php endforeach ?>
				                        </select>
				                    </div>
				                    <div class="col-sm-12">
				                        <label class="col-form-label col-2">Jam</label>
				                        <input type="time" name="jam" class="form-control">
				                    </div>
				                    
				                    <div class="clearfix"></div>
				                </div>
				                <div class="modal-footer">
				                    <div class="table-action-box">
				                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				                        <button type="submit" class="btn btn-primary">Simpan</button>
				                    </div>
				                </div>
				            </form>
				        </div>
				    </div>
				</div>
			</div>
		</div>

    <?php include "template/scripts.php"; ?>

</body>

</html>
<script type="text/javascript">
	function tambahModal() {
        $("#modalImage").modal('show');
        $("#modalImage").find('.modal-title').text('Tambah Data');
        $("#modalImage").find('form').find('input').val('');
        $("#modalImage").find('form').attr('action','jadwal_pelajaran/do_input');
        $("input[name='kelas_map_id']").val('<?=$kelas[0]['kelas_map_id']?>');
    }

    function updateModal(jadwal_pelajaran_id) {
        $("#modalImage").modal('show');
        $("#modalImage").find('.modal-title').text('Edit Data');
        $("#modalImage").find('form').find('input').val('');
        $("#modalImage").find('form').attr('action','jadwal_pelajaran/do_update');
        var all = JSON.stringify(<?php echo json_encode($jadwal_pelajaran) ?>);
        var obj = jQuery.parseJSON(all);
        $("input[name='jadwal_pelajaran_id']").val(obj[jadwal_pelajaran_id].jadwal_pelajaran_id);
        $("select[name='kelas_map_pelajaran_id']").val(obj[jadwal_pelajaran_id].kelas_map_pelajaran_id).attr('selected', 'selected');
        $("select[name='hari_id']").val(obj[jadwal_pelajaran_id].hari_id).attr('selected', 'selected');
        $("input[name='jam']").val(obj[jadwal_pelajaran_id].jam);
    }
</script>