<!DOCTYPE html>
<html>

<head>

<?php include "template/head.php"; ?>

</head>

<body>
    <?php include "template/topbar.php"; ?>

    <div class="parent-wrapper toggled" id="outer-wrapper">

        <?php include "template/sidebar.php"; ?>

        <!-- MAIN CONTENT -->
        <div class="main-content" id="content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <h5 class="page-title"><i class="fa fa-users"></i><?php echo $title ?></h5>
                        <div class="section-divider"></div>       
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-lg-12 clear-padding-xs">
                        <div class="col-lg-12">
                            <div class="dash-item first-dash-item">
                                <div class="inner-item" style="overflow-x: auto;">
                                    <?php
                                    $error = $this->session->flashdata('error');
                                    if ($error) {
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <?php echo $error; ?> <a href="#" class="alert-link">Error!</a>.
                                        </div> 
                                    <?php } ?>
                                    <?php
                                    $success = $this->session->flashdata('success');
                                    if ($success) {
                                        ?>
                                        <div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <?php echo $success; ?> <a href="#" class="alert-link">Success!</a>.
                                        </div> 
                                    <?php } ?> 
                                    <a class="btn btn-primary pull-right" onclick="tambahModal()">Tambah</a><br><br>
                                    <table id="attendenceDetailedTable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>NIK</th>
                                                <th>Nama</th>
                                                <th>Tanggal Lahir</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Jabatan</th>
                                                <th>Agama</th>
                                                <th>Email</th>
                                                <th>Telp</th>
                                                <th>Telegram</th>
                                                <th>Alamat</th>
                                                <th>Status Aktif</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        foreach($karyawan as $data) {
                                        echo "<tr>
                                                <td>".$no."</td>
                                                <td>".$data['nik']."</td>
                                                <td>".$data['nama']."</td>
                                                <td>".$data['tanggal_lahir']."</td>
                                                <td>".$data['jenis_kelamin_name']."</td>
                                                <td>".$data['jabatan_name']."</td>
                                                <td>".$data['agama_name']."</td>
                                                <td>".$data['email']."</td>
                                                <td>".$data['no_telp']."</td>
                                                <td>".$data['telegram']."</td>
                                                <td>".$data['alamat']."</td>
                                                <td>".$data['name_is_active']."</td>
                                                <td class='action-link'>
                                                    <a class='edit' href='#' title='Edit' onclick='updateModal(".$data['karyawan_id'].")'><i class='fa fa-edit'></i></a>
                                                    <a class='delete' href='karyawan/do_delete/".$data['karyawan_id']."' title='Delete' onclick='return confirm()'><i class='fa fa-remove'></i></a>
                                                </td>
                                            </tr>";
                                            $no++;
                                        }; ?>
                                        </tbody>
                                        </table>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-togggle-btn">
                <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
            </div>
            <div class="dash-footer col-lg-12">
                <p>SD Kristen Sokaraja</p>
            </div>
        </div>
    </div>
    <?php include "template/scripts.php"; ?>
</body>
</html>
<!--Kelas karyawan-->
<div id="modalImage" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="karyawan_kelas" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></i></h4>
                </div>
                    <input type="hidden" class="form-control" name="karyawan_id" required="">
                <div class="modal-body">
                    <div class="col-md-6">
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">NIK</label>
                            <input type="number" class="form-control col-9" name="nik" required="">
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Nama</label>
                            <input type="text" class="form-control col-9" name="nama" required="">
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Password</label>
                            <input type="password" class="form-control col-9" name="password" required="">
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Tanggal Lahir</label>
                            <input type="date" class="form-control col-9" name="tanggal_lahir" required="">
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Jenis Kelamin</label>
                            <select class="form-control col-9" name="jenis_kelamin_id" required="">
                              <?php foreach ($list_jenis_kelamin as $value): ?>
                                <option value="<?php echo $value['data_combo_id'] ?>"><?php echo $value['nama'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Jabatan</label>
                            <select class="form-control col-9" name="jabatan_id" required="">
                              <?php foreach ($list_jabatan as $value): ?>
                                <option value="<?php echo $value['data_combo_id'] ?>"><?php echo $value['nama'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Agama</label>
                            <select class="form-control col-9" name="agama_id" required="">
                              <?php foreach ($list_agama as $value): ?>
                                <option value="<?php echo $value['data_combo_id'] ?>"><?php echo $value['nama'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Email</label>
                            <input type="email" class="form-control col-9" name="email" required="">
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Telp</label>
                            <input type="text" class="form-control col-9" name="no_telp" required="">
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Telegram</label>
                            <input type="text" class="form-control col-9" name="telegram" required="">
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Alamat</label>
                            <input type="text" class="form-control col-9" name="alamat" required="">
                        </div>
                        <div class="col-sm-12">
                            <label class="col-form-label col-2">Status Aktif</label>
                            <select class="form-control col-9" name="status_id" required="">
                              <?php foreach ($list_is_active as $value): ?>
                                <option value="<?php echo $value['data_combo_id'] ?>"><?php echo $value['nama'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <div class="table-action-box">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#mytable").DataTable();
    });

    function tambahModal() {
        $("#modalImage").modal('show');
        $("#modalImage").find('.modal-title').text('Tambah Data');
        $("#modalImage").find('form').find('input').val('');
        $("#modalImage").find('form').attr('action','karyawan/do_input');
        $("input[name='password']").attr('required', 'required').removeAttr('placeholder');
    }

    function updateModal(karyawan_id) {
        $("#modalImage").modal('show');
        $("#modalImage").find('.modal-title').text('Edit Data');
        $("#modalImage").find('form').find('input').val('');
        $("#modalImage").find('form').attr('action','karyawan/do_update');
        var all = JSON.stringify(<?php echo json_encode($karyawan) ?>);
        var obj = jQuery.parseJSON(all);
        $("input[name='karyawan_id']").val(obj[karyawan_id].karyawan_id);
        $("input[name='nik']").val(obj[karyawan_id].nik);
        $("input[name='nama']").val(obj[karyawan_id].nama);
        $("input[name='tanggal_lahir']").val(obj[karyawan_id].tanggal_lahir);
        $("input[name='email']").val(obj[karyawan_id].email);
        $("input[name='no_telp']").val(obj[karyawan_id].no_telp);
        $("input[name='telegram']").val(obj[karyawan_id].telegram);
        $("input[name='alamat']").val(obj[karyawan_id].alamat);
        $("select[name='jabatan_id']").val(obj[karyawan_id].jabatan_id).attr('selected', 'selected');
        $("select[name='jenis_kelamin_id']").val(obj[karyawan_id].jenis_kelamin_id).attr('selected', 'selected');
        $("select[name='agama_id']").val(obj[karyawan_id].agama_id).attr('selected', 'selected');
        $("select[name='status_id']").val(obj[karyawan_id].status_id).attr('selected', 'selected');
        $("input[name='password']").removeAttr('required').attr('placeholder', 'Isikan jika ingin dirubah');
    }
</script>