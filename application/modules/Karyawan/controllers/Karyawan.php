<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends MX_Controller {
	public function __construct()
  	{
    	parent::__construct();
    	$this->c_auth->checkLogin();
			$this->admin = $this->c_auth->getAdmin();
			$this->global = $this->c_global->data_list();
			$this->global_kelas_pelajaran = $this->c_global->kelas_pelajaran();
			$this->global_semester = $this->c_global->list_semester();
    	$this->load->model('m_karyawan');
  	}

  	public function index() {
		$data['admin'] = $this->admin;
		$data['global'] = $this->global;
		$data['kelas_pelajaran'] = $this->global_kelas_pelajaran;
		$data['semester'] = $this->global_semester;		
		$data['title'] = 'Master Karyawan';
	    $data['karyawans'] = $this->m_karyawan->data_karyawan();
	    foreach ($data['karyawans'] as $key => $val) {
	      	$data['karyawan'][$val['karyawan_id']] = $val;
      		unset($val['karyawan'][$key]);	
	    }
	    $data['list_is_active'] = $this->m_karyawan->get_list_is_active();
	    $data['list_agama'] = $this->m_karyawan->get_list_agama();
	    $data['list_jenis_kelamin'] = $this->m_karyawan->get_list_jenis_kelamin();
	    $data['list_jabatan'] = $this->m_karyawan->get_list_jabatan();
		$this->load->view('karyawan',$data);
	}

	public function do_input(){
	    $post = $this->input->post();
	    $insert = array(
			'nik' 				=> $post['nik'],
			'nama'         		=> $post['nama'],
			'tanggal_lahir' 	=> $post['tanggal_lahir'],
			'jenis_kelamin_id' 	=> $post['jenis_kelamin_id'],
			'email' 			=> $post['email'],
			'alamat' 			=> $post['alamat'],
			'telegram' 			=> $post['telegram'],
			'jabatan_id' 		=> $post['jabatan_id'],
			'no_telp' 			=> $post['no_telp'],
			'agama_id' 			=> $post['agama_id'],
			'status_id' 		=> $post['status_id'],
			'password'			=> $this->encryption->encrypt($post['password'])
	    );

	    $result_insert = $this->m_karyawan->insert($insert);
	    if ($result_insert) {
	      	$this->session->set_flashdata('success', 'Data success insert!');
	    } else {
	        $this->session->set_flashdata('error', 'Data cannot insert!');
	    }
	    redirect(base_url().'karyawan');
  	}

  	public function do_update() {
	    $post = $this->input->post();
	    $id = $post['karyawan_id'];
	    $data_update = array(
			'nik' 				=> $post['nik'],
			'nama'         		=> $post['nama'],
			'tanggal_lahir' 	=> $post['tanggal_lahir'],
			'jenis_kelamin_id' 	=> $post['jenis_kelamin_id'],
			'email' 			=> $post['email'],
			'alamat' 			=> $post['alamat'],
			'telegram' 			=> $post['telegram'],
			'jabatan_id' 		=> $post['jabatan_id'],
			'no_telp' 			=> $post['no_telp'],
			'agama_id' 			=> $post['agama_id'],
			'status_id' 		=> $post['status_id']
	    );

	    if (isset($post['password'])) {
	    	$data_update['password'] = $this->encryption->encrypt($post['password']);
	    }
	    $result_update = $this->m_karyawan->update($id, $data_update);
	    if ($result_update) {
	      	$this->session->set_flashdata('success', 'Data success updated!');
	  	} else {
	        $this->session->set_flashdata('error', 'Data cannot updated!');
	  	}
	    redirect(base_url().'karyawan');
  	}

  	public function do_delete($id) {
	    $result_delete = $this->m_karyawan->delete($id);
	    if ($result_delete) {
	      	$this->session->set_flashdata('success', 'Data success deleted!');
	  	} else {
		    $this->session->set_flashdata('error', 'Data cannot deleted!');
	  	}
	    redirect(base_url().'karyawan');
  	}
}
