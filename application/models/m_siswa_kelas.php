<?php
class m_siswa_kelas extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }      

    public function get_list_siswa_kelas($tahun_ajar_id, $kelas_id) {
        $q="
        SELECT 
            s.`siswa_id`,
            s.`nama`,
        CASE
            WHEN onclass.siswa_id IS NOT NULL THEN 'checked'
            ELSE 'uncheck'
        END AS stat
        FROM siswa s
        LEFT JOIN (
                            SELECT s.siswa_id
                            FROM siswa s
                            LEFT JOIN kelas_map_siswa kms ON (s.siswa_id=kms.siswa_id)
                            LEFT JOIN kelas_map km ON (km.kelas_map_id=kms.kelas_map_id)
                            WHERE status_id = 13
                            AND km.tahun_ajar_id = '".$tahun_ajar_id."'
                            AND km.kelas_id = '".$kelas_id."'
                            ) onclass ON (onclass.siswa_id=s.siswa_id)
        WHERE s.status_id = 13
        ORDER BY stat, s.nama
        ";
        return $this->db->query($q)->result_array();     
    } 

    public function insert($insert) {
        $this->db->insert('kelas_map_siswa', $insert);
        return true;
    }

    public function delete($delete) {
        $this->db->delete('kelas_map_siswa', $delete);
        return true;
    }

    public function get_list_siswa($tahun_ajar_id, $kelas_id) {
        $q="
            SELECT s.siswa_id,
            s.nama,
            s.nis,
            k.nama as kelas
            FROM siswa s
            LEFT JOIN kelas_map_siswa kms ON (s.siswa_id=kms.siswa_id)
            LEFT JOIN kelas_map km ON (km.kelas_map_id=kms.kelas_map_id)
            left join kelas k on (k.kelas_id=km.kelas_id)
            WHERE s.status_id = 13
            AND km.tahun_ajar_id = '".$tahun_ajar_id."'
            AND km.kelas_map_id = '".$kelas_id."'
        ";
        return $this->db->query($q)->result_array();     
    } 

}