<?php
class m_tahun_ajar extends CI_Model {

    public function __construct()
    {
            parent::__construct();
            // Your own constructor code
    }       

   

    public function data_tahun_ajar() {
        $q="
        SELECT 
        tahun_ajar.*,
            data_combo.`nama` AS `name_is_active`
        FROM tahun_ajar
        LEFT JOIN data_combo ON data_combo.`data_combo_id` = tahun_ajar.`status_id`
        ";
        return $this->db->query($q)->result_array();     
    }     

    public function get_kelas_by_id($id){
        $q="
        SELECT * FROM kelas WHERE kelas_id = $id
        ";
        return $this->db->query($q)->result_array();     
    }

    public function get_list_is_active() {
        $q="
        SELECT *
        FROM data_combo
        WHERE data_combo_id = 12 OR data_combo_id = 13
        ";
        return $this->db->query($q)->result_array();     
    }

    public function insert($insert) {
        $this->db->insert('tahun_ajar', $insert);
        return true;
    }

    public function update($id, $data_update) {
        $this->db->where('tahun_ajar_id', $id);
        $this->db->update('tahun_ajar', $data_update);
        return true;
    }

    public function delete($id) {
        $this->db->delete('tahun_ajar', array('tahun_ajar_id' => $id));
        return true;
    }
}