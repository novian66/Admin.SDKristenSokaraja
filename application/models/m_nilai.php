<?php
class m_nilai extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }       

    public function get_kelas_pelajaran($kelas_map_pelajaran_id) {
        $q="
        SELECT
            a.*,
            b.`nama` AS `pelajaran_name`,
            d.`nama` AS `kelas_name`,
            e.`nama` AS `tahun_ajar_name`
        FROM kelas_map_pelajaran a
        LEFT JOIN pelajaran b ON b.`pelajaran_id` = a.`pelajaran_id`
        LEFT JOIN kelas_map c ON c.`kelas_map_id` = a.`kelas_map_id`
        LEFT JOIN kelas d ON d.`kelas_id` = c.`kelas_id`
        LEFT JOIN tahun_ajar e ON e.`tahun_ajar_id` = c.`tahun_ajar_id`
        WHERE a.`kelas_map_pelajaran_id` = '".$kelas_map_pelajaran_id."'
        ";
        return $this->db->query($q)->result_array();	 
    }  

    public function get_semester($semester_id) {
        $q="
        SELECT *
        FROM data_combo
        WHERE data_combo_id = '".$semester_id."'
        ";
        return $this->db->query($q)->result_array();
    }

    public function get_nilai($kelas_map_pelajaran_id, $semester_id) {
        $q="
        SELECT 
            s.siswa_id,
            s.nis,
            s.nama,
            COALESCE(n.nilai,0) nilai,
            COALESCE(n.keterangan,'') keterangan
        FROM kelas_map_pelajaran kmp
        LEFT JOIN kelas_map km ON (km.kelas_map_id=kmp.kelas_map_id)
        LEFT JOIN kelas_map_siswa kms ON (kms.kelas_map_id=km.kelas_map_id)
        LEFT JOIN (SELECT * FROM nilai 
                                WHERE
                                kelas_map_pelajaran_id = '".$kelas_map_pelajaran_id."'
                                AND semester_id = '".$semester_id."'
                            ) n ON (n.siswa_id=kms.siswa_id)
        LEFT JOIN siswa s ON (s.siswa_id=kms.siswa_id)
        WHERE kmp.kelas_map_pelajaran_id = '".$kelas_map_pelajaran_id."'
        ";
        return $this->db->query($q)->result_array();
    }

    public function raport($siswa_id,$semester_id,$tahun_ajar_id){
        $q="
        select 
        p.nama as pelajaran,
        kmp.kkm,
        n.nilai,
        n.keterangan
        from nilai n
        left join kelas_map_pelajaran kmp on (kmp.kelas_map_pelajaran_id=n.kelas_map_pelajaran_id)
        left join kelas_map km on (km.kelas_map_id=kmp.kelas_map_id)
        left join pelajaran p on (p.pelajaran_id=kmp.pelajaran_id)
        where n.siswa_id = $siswa_id
        and semester_id = $semester_id
        and tahun_ajar_id = $tahun_ajar_id
        order by p.nama
        ";
        return $this->db->query($q)->result_array();
    }

    public function update($data_update) {
        $q=$this->db->query("
            INSERT INTO `nilai`
                (
                    `kelas_map_pelajaran_id`,
                    `semester_id`,
                    `siswa_id`,
                    `nilai`,
                    `keterangan`
                )
            values (
                '".$data_update['kelas_map_pelajaran_id']."',
                '".$data_update['semester_id']."',
                '".$data_update['siswa_id']."',
                '".$data_update['nilai']."',
                '".$data_update['keterangan']."'
            )
            ON DUPLICATE KEY UPDATE
                `nilai`=values(`nilai`),
                `keterangan`=values(`keterangan`);
        ");
        if ($q) {
            return true;
        }
    }
}