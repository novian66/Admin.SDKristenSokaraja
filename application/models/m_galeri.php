<?php
class m_galeri extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }       

    public function data_galeri() {
        $q="
        SELECT 
            *
        FROM galeri
        ";
        return $this->db->query($q)->result_array();	 
    }     

    public function get_galeri_by_id($id){
        $q="
        SELECT * FROM galeri WHERE galeri_id = $id
        ";
        return $this->db->query($q)->result_array();	 
    }

    public function insert($insert) {
        $this->db->insert('galeri', $insert);
        return true;
    }

    public function update($id, $data_update) {
        $this->db->where('galeri_id', $id);
        $this->db->update('galeri', $data_update);
        return true;
    }

    public function delete($id) {
        $this->db->delete('galeri', array('galeri_id' => $id));
        return true;
    }
}