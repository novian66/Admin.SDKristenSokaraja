<?php
class m_product_category extends CI_Model {

        public function __construct()
        {
                parent::__construct();
                // Your own constructor code
        }       

        public function data_product_category() {
                $q="
                select *, 
                (select name from data_list where data_list_id = category.is_active) AS `name_is_active`,
                (select name from data_list where data_list_id = category.is_show) AS `name_is_show`
                from category
                ";
                return $this->db->query($q)->result_array();	 
        }     

        public function get_category_by_product_id($id){
                $q="
                select * from product_category where product_id = $id
                ";
                return $this->db->query($q)->result_array();	 
        }

        public function get_list_is_active() {
                $q="
                select *
                from data_list
                where is_active = 1000001 and group_list = 'is_active'
                ";
                return $this->db->query($q)->result_array();     
        }

        public function insert($insert) {
                $this->db->insert('category', $insert);
                return true;
        }

        public function update($id, $data_update) {
                $this->db->where('category_id', $id);
                $this->db->update('category', $data_update);
                return true;
        }

        public function delete($id) {
                $this->db->delete('category', array('category_id' => $id));
                return true;
        }
}