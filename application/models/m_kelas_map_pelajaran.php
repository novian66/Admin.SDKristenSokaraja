<?php
class m_kelas_map_pelajaran extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }      

    public function get_list_kelas_map_pelajaran($kelas_map_pelajaran_id) {
        $q="
        SELECT
            a.*,
            b.`nama` AS `pelajaran_name`,
            c.`nama` AS `karyawan_name`
        FROM kelas_map_pelajaran a
        LEFT JOIN pelajaran b ON b.`pelajaran_id` = a.`pelajaran_id`
        LEFT JOIN karyawan c ON c.`karyawan_id` = a.`karyawan_id`
        WHERE a.`kelas_map_id` = '".$kelas_map_pelajaran_id."'
        ";
        return $this->db->query($q)->result_array();     
    } 

    public function get_list_pelajaran() {
        $q="
        SELECT *
        FROM pelajaran
        WHERE status_id = 13
        ";
        return $this->db->query($q)->result_array();     
    }

    public function get_list_karyawan() {
        $q="
        SELECT *
        FROM karyawan
        WHERE status_id = 13
        ";
        return $this->db->query($q)->result_array();     
    }

    public function insert($insert) {
        $this->db->insert('kelas_map_pelajaran', $insert);
        return true;
    }

    public function update($id, $data_update) {
        $this->db->where('kelas_map_pelajaran_id', $id);
        $this->db->update('kelas_map_pelajaran', $data_update);
        return true;
    }

    public function delete($id) {
        $this->db->delete('kelas_map_pelajaran', array('kelas_map_pelajaran_id' => $id));
        return true;
    }
}