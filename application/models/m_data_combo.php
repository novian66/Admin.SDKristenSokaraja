<?php
class m_data_combo extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }       

    public function data_data_combo() {
        $q="
        SELECT 
            *
        FROM data_combo
        ";
        return $this->db->query($q)->result_array();	 
    }     

    public function get_data_combo_by_id($id){
        $q="
        SELECT * FROM data_combo WHERE data_combo_id = $id
        ";
        return $this->db->query($q)->result_array();	 
    }

    public function insert($insert) {
        $this->db->insert('data_combo', $insert);
        return true;
    }

    public function update($id, $data_update) {
        $this->db->where('data_combo_id', $id);
        $this->db->update('data_combo', $data_update);
        return true;
    }

    public function delete($id) {
        $this->db->delete('data_combo', array('data_combo_id' => $id));
        return true;
    }
}