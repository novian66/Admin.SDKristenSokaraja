<?php
class m_jadwal_pelajaran extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }      

    public function get_jadwal_pelajaran($kelas_map_id) {
        $q="
        SELECT
            d.*,
            a.*,
            b.`nama` AS `pelajaran_name`,
            c.`nama` AS `karyawan_name`,
            e.`nama` AS `hari_name`
        FROM jadwal_pelajaran d
        LEFT JOIN data_combo e ON e.`data_combo_id` = d.`hari_id`
        LEFT JOIN kelas_map_pelajaran a ON d.`kelas_map_pelajaran_id` = a.`kelas_map_pelajaran_id`
        LEFT JOIN pelajaran b ON b.`pelajaran_id` = a.`pelajaran_id`
        LEFT JOIN karyawan c ON c.`karyawan_id` = a.`karyawan_id`
        WHERE a.`kelas_map_id` = '".$kelas_map_id."'
        ";
        return $this->db->query($q)->result_array();     
    } 

    public function get_list_hari() {
        $q="
        SELECT *
        FROM data_combo
        WHERE group_list = 'hari'
        ";
        return $this->db->query($q)->result_array();     
    }

    public function get_list_kelas_map_pelajaran($kelas_map_id) {
        $q="
        SELECT 
            a.`kelas_map_pelajaran_id`,
            b.`nama` AS `pelajaran_name`,
            c.`nama` AS `karyawan_name`
        FROM kelas_map_pelajaran a
        LEFT JOIN pelajaran b ON b.`pelajaran_id` = a.`pelajaran_id`
        LEFT JOIN karyawan c ON c.`karyawan_id` = a.`karyawan_id`
        WHERE a.`kelas_map_id` = '".$kelas_map_id."'
        ";
        return $this->db->query($q)->result_array();     
    }

    public function insert($insert) {
        $this->db->insert('jadwal_pelajaran', $insert);
        return true;
    }

    public function update($id, $data_update) {
        $this->db->where('jadwal_pelajaran_id', $id);
        $this->db->update('jadwal_pelajaran', $data_update);
        return true;
    }

    public function delete($id) {
        $this->db->delete('jadwal_pelajaran', array('jadwal_pelajaran_id' => $id));
        return true;
    }
}