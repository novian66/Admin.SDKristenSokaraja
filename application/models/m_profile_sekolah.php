<?php
class m_profile_sekolah extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }       

    public function data_profile_sekolah() {
        $q="
        SELECT 
            *
        FROM profil
        ";
        return $this->db->query($q)->result_array();	 
    }  

    public function update($id, $data_update) {
        $this->db->where('profil_id', $id);
        $this->db->update('profil', $data_update);
        return true;
    }
}