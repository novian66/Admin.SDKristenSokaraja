<?php
class m_informasi extends CI_Model {

    public function __construct()
    {
            parent::__construct();
            // Your own constructor code
    }       

    public function data_informasi() {
            $q="
            SELECT 
                a.*,
                COALESCE(c.`nama`, 'Semua') AS `kelas_name`,
                COALESCE(d.`nama`, 'Semua') AS `tahun_ajar_name`,
                e.`nama` AS `name_is_active`
            FROM informasi a
            LEFT JOIN kelas_map b ON b.`kelas_map_id` = a.`kelas_map_id` 
            LEFT JOIN kelas c ON c.`kelas_id` = b.`kelas_id` 
            LEFT JOIN tahun_ajar d ON d.`tahun_ajar_id` = b.`tahun_ajar_id` 
            LEFT JOIN data_combo e ON e.`data_combo_id` = a.`status_id`
            ";
            return $this->db->query($q)->result_array();	 
    }   
    
    public function get_list_is_active() {
        $q="
        SELECT *
        FROM data_combo
        WHERE data_combo_id = 12 OR data_combo_id = 13
        ";
        return $this->db->query($q)->result_array();     
    }

    public function get_list_kelas() {
        $q="
        SELECT *
        FROM kelas";
        return $this->db->query($q)->result_array();     
    }

    public function get_list_kelas_map() {
        $q="
        SELECT 
            b.*,
            COALESCE(c.`nama`, 'Semua') AS `kelas_name`,
            COALESCE(d.`nama`, 'Semua') AS `tahun_ajar_name`
        FROM kelas_map b
        LEFT JOIN kelas c ON c.`kelas_id` = b.`kelas_id` 
        LEFT JOIN tahun_ajar d ON d.`tahun_ajar_id` = b.`tahun_ajar_id` 
        where b.tahun_ajar_id = ".$this->session->userdata('tahun_ajar_id');
        return $this->db->query($q)->result_array();     
    }

    public function insert($insert) {
        $this->db->insert('informasi', $insert);
        return true;
    }

    public function update($id, $data_update) {
        $this->db->where('informasi_id', $id);
        $this->db->update('informasi', $data_update);
        return true;
    }

    public function delete($id) {
        $this->db->delete('informasi', array('informasi_id' => $id));
        return true;
    }

    public function get_target($kelas_map_id=''){
        $str = '';
        if(!empty($kelas_map_id)){
            $str = " and km.kelas_map_id = $kelas_map_id";
        }
        $q="
            select k.nama as kelas,s.* from kelas_map_siswa kmp 
            left join kelas_map km  on (kmp.kelas_map_id=km.kelas_map_id)
            left join siswa s on (s.siswa_id=kmp.siswa_id)
            left join kelas k on (km.kelas_id=k.kelas_id)
            where km.tahun_ajar_id = 1
            and s.status_id = 13
            and telegram is not null      
            $str  
        ";
        return $this->db->query($q)->result_array(); 
    }
}