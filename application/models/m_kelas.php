<?php
class m_kelas extends CI_Model {

    public function __construct()
    {
            parent::__construct();
            // Your own constructor code
    }       

    public function data_wali_kelas($id) {
            $q="
            select 
            kelas_map.kelas_map_id,
            kelas.kelas_id,
            $id as 'tahun_ajar_id',
            kelas_map.karyawan_id, 
            kelas.nama as kelas,
            kelas_map.karyawan
            
            from 
            kelas 
            left join (
                select 
                km.*,
                kelas.nama as kelas,
                karyawan.nama as karyawan
                from 
                kelas_map km
                left join kelas on (kelas.kelas_id=km.kelas_id)
                left join karyawan on (karyawan.karyawan_id=km.karyawan_id)
                where km.tahun_ajar_id = $id
                order by kelas.nama
                                    ) kelas_map on (kelas_map.kelas_id=kelas.kelas_id)
            where status_id = 13
            ";
            return $this->db->query($q)->result_array();	 
    }   
    
    public function get_kelas_by_kelas_map_id($id) {
            $q="
            select * from kelas_map km
            left join kelas on (kelas.kelas_id=km.kelas_id)
            where kelas_map_id = $id
            ";
            return $this->db->query($q)->result_array();	 
    } 

    public function data_kelas() {
        $q="
        SELECT 
            kelas.*,
            data_combo.`nama` AS `name_is_active`
        FROM kelas
        LEFT JOIN data_combo ON data_combo.`data_combo_id` = kelas.`status_id`
        ";
        return $this->db->query($q)->result_array();     
    }     

    public function get_kelas_by_id($id){
        $q="
        SELECT * FROM kelas WHERE kelas_id = $id
        ";
        return $this->db->query($q)->result_array();     
    }

    public function get_list_is_active() {
        $q="
        SELECT *
        FROM data_combo
        WHERE data_combo_id = 12 OR data_combo_id = 13
        ";
        return $this->db->query($q)->result_array();     
    }

    public function insert($insert) {
        $this->db->insert('kelas', $insert);
        return true;
    }

    public function update($id, $data_update) {
        $this->db->where('kelas_id', $id);
        $this->db->update('kelas', $data_update);
        return true;
    }

    public function delete($id) {
        $this->db->delete('kelas', array('kelas_id' => $id));
        return true;
    }
}