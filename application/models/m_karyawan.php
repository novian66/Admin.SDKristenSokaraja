<?php
class m_karyawan extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }       

    public function data_karyawan() {
        $q="
        SELECT 
            a.*,
            b.`nama` AS `name_is_active`,
            c.`nama` AS `agama_name`,
            d.`nama` AS `jabatan_name`,
            e.`nama` AS `jenis_kelamin_name`
        FROM karyawan a
        LEFT JOIN data_combo b ON b.`data_combo_id` = a.`status_id`
        LEFT JOIN data_combo c ON c.`data_combo_id` = a.`agama_id`
        LEFT JOIN data_combo d ON d.`data_combo_id` = a.`jabatan_id`
        LEFT JOIN data_combo e ON e.`data_combo_id` = a.`jenis_kelamin_id`
        ";
        return $this->db->query($q)->result_array();	 
    }     

    public function get_karyawan_by_id($id, $tahun_ajar_id){
        $q="
        SELECT 
            a.*,
            b.`nama` AS `name_is_active`,
            c.`nama` AS `agama_name`,
            d.`nama` AS `jabatan_name`,
            e.`nama` AS `jenis_kelamin_name`,
            (
                SELECT 
                    GROUP_CONCAT(g.`nama`) 
                FROM kelas_map f 
                LEFT JOIN kelas g ON g.`kelas_id` = f.`kelas_id` 
                WHERE f.`karyawan_id` = a.`karyawan_id` 
                AND f.`tahun_ajar_id` = '".$tahun_ajar_id."'
            ) AS kelas,
            (
                SELECT 
                    GROUP_CONCAT(DISTINCT(g.`nama`))
                FROM pelajaran g
                LEFT JOIN kelas_map_pelajaran h ON h.`pelajaran_id` = g.`pelajaran_id`
                LEFT JOIN kelas_map i ON i.`kelas_map_id` = h.`kelas_map_id`
                WHERE h.`karyawan_id` = a.`karyawan_id` 
                AND i.`tahun_ajar_id` = '".$tahun_ajar_id."'
            ) AS pelajaran
        FROM karyawan a
        LEFT JOIN data_combo b ON b.`data_combo_id` = a.`status_id`
        LEFT JOIN data_combo c ON c.`data_combo_id` = a.`agama_id`
        LEFT JOIN data_combo d ON d.`data_combo_id` = a.`jabatan_id`
        LEFT JOIN data_combo e ON e.`data_combo_id` = a.`jenis_kelamin_id`
        WHERE karyawan_id = '".$id."'
        ";
        return $this->db->query($q)->result_array();	 
    }

    public function get_list_is_active() {
        $q="
        SELECT *
        FROM data_combo
        WHERE data_combo_id = 12 OR data_combo_id = 13
        ";
        return $this->db->query($q)->result_array();     
    }

    public function get_list_agama() {
        $q="
        SELECT *
        FROM data_combo
        WHERE group_list = 'agama'
        ";
        return $this->db->query($q)->result_array();     
    }

    public function get_list_jenis_kelamin() {
        $q="
        SELECT *
        FROM data_combo
        WHERE group_list = 'jk'
        ";
        return $this->db->query($q)->result_array();     
    }

    public function get_list_jabatan() {
        $q="
        SELECT *
        FROM data_combo
        WHERE group_list = 'karyawan'
        ";
        return $this->db->query($q)->result_array();     
    }

    public function insert($insert) {
        $this->db->insert('karyawan', $insert);
        return true;
    }

    public function update($id, $data_update) {
        $this->db->where('karyawan_id', $id);
        $this->db->update('karyawan', $data_update);
        return true;
    }

    public function delete($id) {
        $this->db->delete('karyawan', array('karyawan_id' => $id));
        return true;
    }
}