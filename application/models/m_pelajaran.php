<?php
class m_pelajaran extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }       

    public function data_pelajaran() {
        $q="
        SELECT 
            pelajaran.*,
            data_combo.`nama` AS `name_is_active`
        FROM pelajaran
        LEFT JOIN data_combo ON data_combo.`data_combo_id` = pelajaran.`status_id`
        ";
        return $this->db->query($q)->result_array();	 
    }     

    public function get_pelajaran_by_id($id){
        $q="
        SELECT * FROM pelajaran WHERE pelajaran_id = $id
        ";
        return $this->db->query($q)->result_array();	 
    }

    public function get_list_is_active() {
        $q="
        SELECT *
        FROM data_combo
        WHERE data_combo_id = 12 OR data_combo_id = 13
        ";
        return $this->db->query($q)->result_array();     
    }

    public function insert($insert) {
        $this->db->insert('pelajaran', $insert);
        return true;
    }

    public function update($id, $data_update) {
        $this->db->where('pelajaran_id', $id);
        $this->db->update('pelajaran', $data_update);
        return true;
    }

    public function delete($id) {
        $this->db->delete('pelajaran', array('pelajaran_id' => $id));
        return true;
    }
}