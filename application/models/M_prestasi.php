<?php
class m_prestasi extends CI_Model {

    public function __construct()
    {
            parent::__construct();
            // Your own constructor code
    }       

    

    public function data_prestasi() {
        $q="
        SELECT 
            prestasi.*,
            data_combo.`nama` AS `name_is_active`
        FROM prestasi
        LEFT JOIN data_combo ON data_combo.`data_combo_id` = prestasi.`status_id`
        ";
        return $this->db->query($q)->result_array();     
    }     


    public function get_list_is_active() {
        $q="
        SELECT *
        FROM data_combo
        WHERE data_combo_id = 12 OR data_combo_id = 13
        ";
        return $this->db->query($q)->result_array();     
    }

    public function insert($insert) {
        $this->db->insert('prestasi', $insert);
        return true;
    }

    public function update($id, $data_update) {
        $this->db->where('prestasi_id', $id);
        $this->db->update('prestasi', $data_update);
        return true;
    }

    public function delete($id) {
        $this->db->delete('prestasi', array('prestasi_id' => $id));
        return true;
    }
}