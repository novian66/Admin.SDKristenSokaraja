<div class="row dashboard-top-nav">
        <div class="col-sm-3 logo">
            <h5><i class="fa fa-book"></i>SD Kristen Sokaraja (<?php echo $admin['tahun_ajar'] ?>)</h5>
        </div>
        <div class="col-sm-4 top-search" style="background:#07253F">
            <div class="search" style="color:#07253F">
                <i class="fa fa-search"></i>
            </div>
        </div>
        <div class="col-sm-5 notification-area">
            <ul class="top-nav-list">
                <li class="user dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span><?php echo $admin['nama'].' ('.$admin['jabatan'].')' ?><span class="caret"></span></span>
                    </a>
                    <ul class="dropdown-menu notification-list">
                        <li>
                            <div class="all-notifications">
                                <a href="login/logout">LOGOUT</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>