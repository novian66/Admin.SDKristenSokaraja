<li>
                    <a href="profile_karyawan"><i class="fa fa-home menu-icon"></i> Lihat & Edit Profil</a>
                </li>
                <li class="dropdown">
                    <a href="profil" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-users menu-icon"></i> Master Data <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="pelajaran">Data Pelajaran</a>
                        </li>
                        <li>
                            <a href="kelas">Data Kelas</a>
                        </li>
                        <li>
                            <a href="karyawan">Data Karyawan</a>
                        </li>
                        <li>
                            <a href="siswa">Data Siswa</a>
                        </li>
                        <li>
                            <a href="data_combo">Data List</a>
                        </li>                                                                                                
                    </ul>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <a href="tahun_ajar"><i class="fa fa-home menu-icon"></i> Tahun Ajar</a>
                </li>                
                <li>
                    <a href="wali_kelas"><i class="fa fa-home menu-icon"></i> Input Wali Kelas</a>
                </li>
                <li>
                    <a href="#" data-toggle="modal" data-target="#kelasPelajaran"><i class="fa fa-home menu-icon"></i> Input Pelajaran Kelas</a>
                </li>                                
                <li>
                    <a href="#" data-toggle="modal" data-target="#siswaKelas"><i class="fa fa-home menu-icon"></i> Input Siswa Kelas</a>
                </li>                                
                <li>
                    <a href="#" data-toggle="modal" data-target="#jadwalPelajaran"><i class="fa fa-home menu-icon"></i> Input Jadwal Kelas</a>
                </li>
                <li>
                    <a href="informasi"><i class="fa fa-home menu-icon"></i> Input Informasi</a>
                </li>
                <li>
                    <a href="galeri"><i class="fa fa-home menu-icon"></i> Input Galeri</a>
                </li>                
                <li>
                    <a href="rapot"><i class="fa fa-home menu-icon"></i> Cetak Raport</a>
                </li>
                <li>
                    <a href="profile_sekolah"><i class="fa fa-home menu-icon"></i> Profil Sekolah</a>
                </li>  