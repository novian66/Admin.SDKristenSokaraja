<!--Kelas Pelajaran-->
<div id="kelasPelajaran" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></i>PILIH KELAS</h4>
            </div>
            <form action="pelajaran_kelas" method="get">
            <div class="modal-body">
                <div class="col-sm-12">
                    <label class="clear-top-margin">KELAS</label>
                    <select name="kelas_map_id" required class="form-control">
                        <option value="">-- Pilih Kelas --</option>
                        <?php foreach($global['kelas'] as $val){
                            echo "<option value='".$val['kelas_map_id']."'>".$val['kelas']."</option>";
                        }?>
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <div class="table-action-box">
                    <input type="submit" class="btn btn-primary" value="LIHAT" />
                    <input type="submit" class="btn btn-primary" data-dismiss="modal" value="TUTUP" />
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!--Jadwal Pelajaran-->
<div id="jadwalPelajaran" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></i>PILIH KELAS</h4>
            </div>
            <form action="jadwal_pelajaran" method="get">
            <div class="modal-body">
                <div class="col-sm-12">
                    <label class="clear-top-margin">KELAS</label>
                    <select name="kelas_map_id" required class="form-control">
                        <option value="">-- Pilih Kelas --</option>
                        <?php foreach($global['kelas'] as $val){
                            echo "<option value='".$val['kelas_map_id']."'>".$val['kelas']."</option>";
                        }?>
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <div class="table-action-box">
                    <input type="submit" class="btn btn-primary" value="LIHAT" />
                    <input type="submit" class="btn btn-primary" data-dismiss="modal" value="TUTUP" />
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!--Siswa Kelas-->
<div id="siswaKelas" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></i>PILIH KELAS</h4>
            </div>
            <form action="siswa_kelas" method="get">
            <div class="modal-body">
                <div class="col-sm-12">
                    <label class="clear-top-margin">KELAS</label>
                    <select name="kelas_map_id" required class="form-control">
                        <option value="">-- Pilih Kelas --</option>
                        <?php foreach($global['kelas'] as $val){
                            echo "<option value='".$val['kelas_map_id']."'>".$val['kelas']."</option>";
                        }?>
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <div class="table-action-box">
                    <input type="submit" class="btn btn-primary" value="LIHAT" />
                    <input type="submit" class="btn btn-primary" data-dismiss="modal" value="TUTUP" />
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!--Nilai Pelajaran-->
<div id="nilaiPelajaran" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></i>PILIH PELAJARAN KELAS</h4>
            </div>
            <form action="nilai" method="get">
            <div class="modal-body">
                <div class="col-sm-12">
                    <label class="clear-top-margin">Pelajaran</label>
                    <select name="kelas_map_pelajaran_id" required class="form-control">
                        <option value="">-- Pilih Pelajaran --</option>
                        <?php foreach($kelas_pelajaran['kelas_pelajaran'] as $val){
                            echo "<option value='".$val['kelas_map_pelajaran_id']."'>(".$val['kelas_name'].") (".$val['pelajaran_name'].")</option>";
                        }?>
                    </select>
                </div>
                <div class="col-sm-12">
                    <label class="clear-top-margin">Semester</label>
                    <select name="semester_id" required class="form-control">
                        <option value="">-- Pilih Semester --</option>
                        <?php foreach($semester['semester'] as $val){
                            echo "<option value='".$val['data_combo_id']."'>".$val['nama']."</option>";
                        }?>
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <div class="table-action-box">
                    <input type="submit" class="btn btn-primary" value="LIHAT" />
                    <input type="submit" class="btn btn-primary" data-dismiss="modal" value="TUTUP" />
                </div>
            </div>
            </form>
        </div>
    </div>
</div>