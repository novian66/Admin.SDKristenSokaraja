<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">
    <title>ADMIN | SD Kristen Sokaraja</title>

    <!-- Styles -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/chartist.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/owl.carousel.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/owl.theme.default.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/style.css" rel="stylesheet" media="screen">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link href="assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
    <link href="assets/js/summernote/dist/summernote.css" rel="stylesheet">